#!/usr/bin/env bash
set -ex

clean_electron_build_leftovers() {
    rm -rf dist
    rm -rf build/application
}

build_electron_application() {
    local electron_builder_image="${1}"; shift
    local electron_builder_image_tag="${1}"; shift

    # Building Electron Application
    docker run --rm -i \
        --env ELECTRON_CACHE="/root/.cache/electron" \
        --env ELECTRON_BUILDER_CACHE="/root/.cache/electron-builder" \
        -v "${PWD}":/project \
        -v ~/.cache/electron:/root/.cache/electron \
        -v ~/.cache/electron-builder:/root/.cache/electron-builder \
        -e TARGET_VERSION_SEMVER="${APP_VERSION}" \
        -e SKIP_APPIMAGE_BUILD="false" \
        -e TARGET_VERSION="${APP_VERSION}" \
        -e COMMAND="sourceonly" \
        "${electron_builder_image}":"${electron_builder_image_tag}" \
        "/project/start-dockerized-builder-task.sh"
}

main() {
    # Image and Tag for electron-builder
    ELECTRON_BUILDER_IMAGE="xivoxc/electron-builder"
    ELECTRON_BUILDER_IMAGE_TAG="2022.03.latest"
    # Build dirs
    WIN_BUILD_DIR="dist/squirrel-windows"
    LIN_BUILD_DIR="dist"

    clean_electron_build_leftovers

    build_electron_application "${ELECTRON_BUILDER_IMAGE}" "${ELECTRON_BUILDER_IMAGE_TAG}"

    mkdir "windows-files"
    tar -C "dist/" -czvf "windows-files/windows-sourceonly.tar.gz" "win-unpacked"

    mkdir "linux-files"
    tar -C "dist/" -czvf "linux-files/linux-sourceonly.tar.gz" "linux-unpacked"
}

APP_VERSION=$(grep -oP '(?<="version": ")([\d\.]+)(?=")' package.json)
main "${@}"
