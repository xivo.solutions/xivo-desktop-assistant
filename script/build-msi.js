const path = require('path');
const MSICreator = require("electron-wix-msi").MSICreator;

async function build() {

  const msiCreator = new MSICreator({
    appDirectory: path.resolve(__dirname, '..', 'xivo-desktop-assistant-win32-x64/'),
    description: 'Desktop application made for XiVO Assistant.',
    exe: 'xivo-desktop-assistant',
    name: 'xivo-desktop-assistant',
    manufacturer: 'Avencall',
    version: '1.0.0',
    appIconPath: path.resolve(__dirname, '..', 'build/icon.png'),
    ui: {
      chooseDirectory: true,
    },
    outputDirectory: path.resolve(__dirname, '..', 'dist/msi-builder')
  });
  await msiCreator.create();
  await msiCreator.compile();
}

build();