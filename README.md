# Electron Application

This is the electron application of XiVO since Luna. For Kuma (2023.05) and before it was in the xucmgt project xivo.solutions/xucmgt> in folder electron.

# Prerequisites #

* NodeJs and NPM :
  * Recommended : Use nvm to deal with the node version, see RandD wiki `Node and NPM using NVM`

## Development

### NodeJS

Currently using node LTS Gallium.

### NPM commands

First you need to initialize dependencies: `npm ci`
If you pulled xucmgt but used to dev on it before we turned electron into TypeScript, make sure to delete your dist folder to prevent typescript compiler errors about old builds leftovers : `rm -rf dist`

Here is a list of the NPM commands that we use the most in various scenarios
- `npm start` -> Starts an instance of electron with your changes (will clean up & compile ts sources)
- `npm run build` -> Clean up and compile ts sources on linux environment only
- `npm run test` -> Runs the unit tests of Electron located in spec/unit (will also clean up & compile ts sources)
- `npm run test:e2e` -> Runs the integration tests of Electron located in spec/e2e (will also clean up & compile ts sources + package the app)
- `npm run test:all` -> Runs both unit and e2e tests (will also clean up & compile ts sources + package the app)

- `npm run dist-packageOnly-linux64` -> Package the app for linux environment
- `npm run dist-linux64` -> Package the app and generate the .deb for linux environment

- `npm run dist-packageOnly-win64` -> Package the app for windows environment
- `npm run dist-win64` -> Package the app and generate the .exe for windows environment

- `npm run build-win64` -> (jenkins) Clean up and compile ts sources on windows environment only
- `npm run dist-buildFromPackage-win64` -> (jenkins) Build the installer using the provided compiled ts sources, useful for signing the sources before building the installer
- `npm run test:e2e-win64` -> (jenkins) Runs the integration tests of Electron located in spec/e2e (will also clean up & compile ts sources + package the app)

The usual dev workflow for linux is to do some modifications, then run `npm start` to try it out.
If it's good, then use `npm run test:all` to check if you did not break anything.

If you want to run the integration tests faster in case you have some of them to fix or new ones to add, you can run
`npm run build && npm run dist-packageOnly-linux64 && TESTDELAY=500 EVALDELAY=500 CLOSEDELAY=500 npx playwright test`
Where TESTDELAY is the delay inbetween each tests, EVALDELAY is the delay inbetween an action and the dom evaluation, and CLOSEDELAY is the delay when closing the app to check for numbers of instances and such things. Values are in ms and 500 for each seems to be the best case scenario if the environment is perfectly stable and has no load.

## Debug using VSCode

* Create a .vscode folder inside this electron folder
* Inside, add a file called launch.json
* Copy-paste the following inside of it:

```json
{
    "version": "0.2.0",
    "configurations": [
      {
        "name": "Debug Main Process",
        "type": "node",
        "request": "launch",
        "cwd": "${workspaceFolder}",
        "runtimeExecutable": "${workspaceFolder}/node_modules/.bin/electron",
        "windows": {
          "runtimeExecutable": "${workspaceFolder}/node_modules/.bin/electron.cmd"
        },
        "args" : ["."],
        "outputCapture": "std", 
        "outFiles": ["${workspaceRoot}/build"],
        "skipFiles": [
            "node_modules/**/*.js",
            "node_modules/**/*.ts",
        ]
      }
    ]
  }
```

* Run `npm run build-n-watch` inside the electron folder to watch-compile typescript sources
* In vscode, click Run > Start Debugging

You should be able to add breakpoints into your js files and hit them. You can press restart on the top right corner 
to take your code into account after your modifications.

## Building the application locally from sources

You will need `nodejs` and `npm` installed on your machine.
To build a Windows `.exe` on Linux, you will need `wine`.

- Go into the `xucmgt/electron` folder
- Install dependencies with `npm clean-install`
- Compile the source code using `npm run build`
- Run the command corresponding to your operating system :
    - For Windows, `npm run dist-win64`, an `.exe` file will be generated in `dist/squirrel-windows`
    - For Debian-based Linux distributions, `npm run dist-linux64`, a `.deb` will be generated in `dist`


## Update

Update mechanism is automatic on windows, when the application starts, it should check if a new version is available and offer to update if one is available.

## Options

Currently only two startup switches are supported:
-d - enable debug tools
--ignore-certificate-errors - activate internal chrome option allowing use with invalid (autosigned) certificates

## Useful technical design informations

### Inter-process communication

Currently, we use Electron IPC to communicate inbetween the different process. Here is a mapping of this whole
system :

![ipc map](./inter_process_communication.png)

## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
