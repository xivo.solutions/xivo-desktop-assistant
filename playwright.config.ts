import { PlaywrightTestConfig } from '@playwright/test'

const config: PlaywrightTestConfig = {
  testDir: './spec/e2e',
  use: {
    headless: true
  },
  testIgnore:'iframeIntegration.spec.ts',
  timeout: 90000,
  fullyParallel: false,
  workers: 1,
  retries: 2,
  expect: {
    timeout: 10000
  }
}

export default config