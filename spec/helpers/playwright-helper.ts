import {
  parseElectronApp,
  ipcMainCallFirstListener
} from 'electron-playwright-helpers'
import { ElectronApplication, Page, _electron as electron } from '@playwright/test'
import { expect } from '@playwright/test'

export default class Helper {

  static debugEnabled = process.env.DEBUG ? process.env.DEBUG as unknown as boolean : false
  static windowsTesting = process.env.WINTEST ? process.env.WINTEST as unknown as boolean : false
  static iframe = process.env.IFRAME ? process.env.IFRAME as unknown as string : "google.com"

  static testDelay = process.env.TESTDELAY ? parseInt(process.env.TESTDELAY as unknown as string) : 3500
  static evalDelay = process.env.EVALDELAY ? parseInt(process.env.EVALDELAY as unknown as string) : 3500
  static closeDelay = process.env.CLOSEDELAY ? parseInt(process.env.CLOSEDELAY as unknown as string) : 3500

  static clickAndWait = async (page: Page, selector: string, delay = 0) => {
    await page.waitForSelector(selector)
    await page.click(selector, {clickCount: 1, force: true});
  }
  
  static genericBeforeEach = async (flags: Array<string> = []) => {

    const appInfo = Helper.windowsTesting ? parseElectronApp("./dist/win-unpacked") : parseElectronApp("./dist/linux-unpacked")

    process.env.CI = 'e2e'
    let electronApp = await electron.launch({
      args: [appInfo.main, ...flags],
      executablePath: appInfo.executable
    })

    if (Helper.debugEnabled) {
    electronApp.on('window', async (page) => {
      console.log(`START - New window opened`)
  
      page.on('pageerror', (error) => {
        console.log(`ERROR - ${error}`)
      })

      page.on('crash', (error) => {
        console.log(`CRASH - Page crashed unexpectedly, most likely because of memory issues`)
      })

      page.on('close', (error) => {
        console.log(`CLOSE - Page closed`)
      })

      page.on('console', (msg) => {
        console.log(`INFO - ${msg.text()}`)
      })
    })      
  }
  
    return electronApp
  
  }

  static delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))
  
  static genericAfterEach = async (electronApp: ElectronApplication, clearLocalStorage = true) => {
      if (electronApp && electronApp.windows().length > 0) {
        return new Promise(async (resolve) => {
          if (clearLocalStorage) await ipcMainCallFirstListener(electronApp, "clearLocalStorage")
          await electronApp.close()
          resolve("Electron app cleaned")
        })
      } else Promise.resolve("No electron app to clean")
  }

  static fillDummyServer = async (page: Page) => {
    // Needed for the save button to trigger an actual reload of the application
    await page.locator("#remoteUrl").fill(Helper.iframe)
  }
  
  static getSizeObject = () => {
    return {
      "application": {
          "ucassistant": {
              "defaultwidth": 470,
              "defaultheight": 774
          },
          "ccagent": {
              "defaultwidth": 362,
              "defaultheight": 824
          },
          "miniccagent": {
              "defaultwidth": 72,
              "defaultheight": 699
          },
          "switchboard": {
              "defaultwidth": 720,
              "defaultheight": 824
          }
      }
    }
  }

  static expectElectronInstancesNb = (app: ElectronApplication, nb: number, timeout: number = 300) => {
    (function oneElectronInstances() {
      if (app.windows().length != nb) setTimeout(oneElectronInstances, timeout)
      else {
        expect(app.windows().length).toBe(nb)
      }
    })()
  }

  static waitForReload = async (app: ElectronApplication, timeout: number = Helper.closeDelay): Promise<Page> => {
    return new Promise((resolve) => {
      setTimeout(async (app) => {
        var page = app.windows()[0]
        await expect(page.locator('.title-bar')).toBeVisible()
        resolve(page)
      }, timeout, app)
    })
  }

} 