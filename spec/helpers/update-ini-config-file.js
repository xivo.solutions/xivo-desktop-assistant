const fs = require('fs');
const path = require('path');
const os = require('os');

function updateConfigFile(values) {
  let configFilePath;

  if (os.platform() === 'win32') {
    configFilePath = path.join(os.homedir(), 'AppData', 'Roaming', 'xivo-desktop-assistant', 'application', 'config', 'xivoconfig.ini');
  } else {
    configFilePath = path.join(os.homedir(), '.config', 'xivo-desktop-assistant', 'application', 'config', 'xivoconfig.ini');
  }

  fs.writeFile(configFilePath, values, 'utf8', (err) => {
    if (err) {
      console.error('Error while writing to file:', err);
    } else {
      console.log('Modifications saved to the file.');
    }
  });
};

module.exports = { updateConfigFile }
