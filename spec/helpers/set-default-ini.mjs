import {updateConfigFile} from './update-ini-config-file.js'

const iniFileTemplate = `
;Defines if the connection is secured or not : HTTPS or HTTP
APP_PROTOCOL=
;Defines the server used by the application : IP or URL
APP_DOMAIN=
;Defines the interface to open by default : UCASSISTANT or CCAGENT
APP_INTERFACE=
;Defines if the UC assistant opens on operating system startup : true or false
APP_STARTUP=
;Defines if the UC assistant will be minimized in the taskbar instead of closed : true or false
APP_CLOSE=
;List of servers (override APP_DOMAIN) : SERVER DISPLAY NAME = SERVER ADDRESS, where server does not include the protocol, one pair per line
[ALT_SERVERS]
`;

updateConfigFile(iniFileTemplate)
