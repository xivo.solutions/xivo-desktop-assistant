import {updateConfigFile} from './update-ini-config-file.js'

const newValues =`
;Defines if the connection is secured or not : HTTPS or HTTP
APP_PROTOCOL=http
;Defines the server used by the application : IP or URL
APP_DOMAIN=
;Defines the interface to open by default : UCASSISTANT or CCAGENT
APP_INTERFACE=
;Defines if the UC assistant opens on operating system startup : true or false
APP_STARTUP=false
;Defines if the UC assistant will be minimized in the taskbar instead of closed : true or false
APP_CLOSE=true
;List of servers (override APP_DOMAIN) : SERVER DISPLAY NAME = SERVER ADDRESS, where server does not include the protocol, one pair per line
[ALT_SERVERS]
dev = dev.dev
test = test.test
`;

updateConfigFile(newValues)
