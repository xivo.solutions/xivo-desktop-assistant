import { expect, test } from '@playwright/test'
import { ElectronApplication, Page, _electron as electron } from '@playwright/test'
import { ipcMainEmit, ipcRendererCallFirstListener } from 'electron-playwright-helpers'
import Helper from '../helpers/playwright-helper';

let electronApp: ElectronApplication
let page: Page

test.beforeEach(async () => {
  await Helper.delay(Helper.testDelay)
  electronApp = await Helper.genericBeforeEach()
})

test.afterEach(async () => { await Helper.genericAfterEach(electronApp) })

test('should go fullscreen and back to its size on jitsi open and close', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  let maximized: boolean = await electronApp.evaluate(async ({ BrowserWindow }, delay) => {
    let mainWindow = BrowserWindow.getAllWindows()[0]
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mainWindow.isMaximized()) }, delay)
    })
  }, Helper.evalDelay)

  expect(maximized).toEqual(false)

  await ipcMainEmit(electronApp, "JITSI_EVENT", "EventStartVideo")

  maximized = await electronApp.evaluate(async ({ BrowserWindow }, delay) => {
    let mainWindow = BrowserWindow.getAllWindows()[0]
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mainWindow.isMaximized()) }, delay)
    })
  }, Helper.evalDelay)

  expect(maximized).toEqual(true)

  ipcMainEmit(electronApp, "JITSI_EVENT", "EventCloseVideo")

  maximized = await electronApp.evaluate(async ({ BrowserWindow }, delay) => {
    let mainWindow = BrowserWindow.getAllWindows()[0]
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mainWindow.isMaximized()) }, delay)
    })
  }, Helper.evalDelay)

  expect(maximized).toEqual(false)

})

test('should send message to webview', async () => {
  page = await electronApp.firstWindow()
  await page.content()
  const data = await ipcRendererCallFirstListener(page, 'msg', 'test.data')
  expect(data).toBe('test.data')
})
