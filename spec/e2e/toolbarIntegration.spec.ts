import { expect, test } from '@playwright/test'
import { ElectronApplication, Page, _electron as electron } from '@playwright/test'
import Helper from '../helpers/playwright-helper';

let electronApp: ElectronApplication

test.beforeEach(async () => {
  await Helper.delay(Helper.testDelay)
  electronApp = await Helper.genericBeforeEach()
})

test.afterEach(async () => { await Helper.genericAfterEach(electronApp) })

let page: Page

test('should navigates correctly when clicking on settings', async () => {
  page = await electronApp.firstWindow()
  await page.waitForSelector('ui-view')
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()
})

test('should go to fullscreen when clicking the fullscreen button', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  const boundsBeforeFullscreen = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  Helper.clickAndWait(page, '.app-fullscreen', Helper.evalDelay)

  const boundsAfterFullscreen = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(boundsAfterFullscreen.width > boundsBeforeFullscreen.width)
  expect(boundsAfterFullscreen.height > boundsBeforeFullscreen.height)
})

test('should go to fullscreen and get back to the previous size on clicking the fullscreen button', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  const boundsBeforeFullscreen = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  Helper.clickAndWait(page, '.app-fullscreen', Helper.evalDelay)
  Helper.clickAndWait(page, '.app-fullscreen', Helper.evalDelay)

  const boundsAfterFullscreen = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(boundsAfterFullscreen).toEqual(boundsBeforeFullscreen)
})

test('should minimize the application when clicking the minimize button', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  await Helper.clickAndWait(page, '.app-min', Helper.evalDelay)

  let infos: {minimized: boolean, focused: boolean} = await electronApp.evaluate(async ({BrowserWindow}) => {
    let mainWindow =  BrowserWindow.getAllWindows()[0]
    return new Promise((resolve) => {
      resolve({minimized: mainWindow.isMinimized(), focused: mainWindow.isFocused()})
    })
  })

  expect(infos.minimized).toEqual(true)
})

test('should close the application when clicking the close button', async () => {
  const page = await electronApp.firstWindow()
  await page.content()

  await Helper.clickAndWait(page, '.app-close', Helper.closeDelay);

  Helper.expectElectronInstancesNb(electronApp, 0)

});