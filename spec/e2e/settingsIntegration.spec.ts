import { expect, test } from '@playwright/test'
import { ElectronApplication, Page, _electron as electron } from '@playwright/test'
import Helper from '../helpers/playwright-helper';
import { ipcMainEmit } from 'electron-playwright-helpers'

let electronApp: ElectronApplication

test.beforeEach(async () => {
  await Helper.delay(Helper.testDelay)
  electronApp = await Helper.genericBeforeEach()
})

test.afterEach(async () => { await Helper.genericAfterEach(electronApp) })

let page: Page
let sizeObject = Helper.getSizeObject()

test('should navigate to settings', async () => {
  page = await electronApp.firstWindow()
  await page.waitForSelector('ui-view')
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()
})

test('should display with the default size of the Switchboard when selecting it', async () => {
  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()

  await page.waitForSelector('#interface-list')
  await page.locator('#interface-list').selectOption('string:switchboard')
  Helper.fillDummyServer(page)
  await page.locator('#save-button').click()

  page = await Helper.waitForReload(electronApp)

  const bounds = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(bounds.width).toEqual(sizeObject.application.switchboard.defaultwidth)
  expect(bounds.height).toEqual(sizeObject.application.switchboard.defaultheight)

})

test('should display with the default size of the CC Agent when selecting it', async () => {
  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()

  await page.waitForSelector('#interface-list')
  await page.locator('#interface-list').selectOption('string:ccagent')
  Helper.fillDummyServer(page)
  await page.locator('#save-button').click()

  page = await Helper.waitForReload(electronApp)

  const bounds = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(bounds.width).toEqual(sizeObject.application.ccagent.defaultwidth)
  expect(bounds.height).toEqual(sizeObject.application.ccagent.defaultheight)

})

test('should display with the default size of the UC Assistant when selecting it', async () => {
  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()

  await page.waitForSelector('#interface-list')
  await page.locator('#interface-list').selectOption('string:ucassistant')
  Helper.fillDummyServer(page)
  await page.locator('#save-button').click()

  page = await Helper.waitForReload(electronApp)

  const bounds = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(bounds.width).toEqual(sizeObject.application.ucassistant.defaultwidth)
  expect(bounds.height).toEqual(sizeObject.application.ucassistant.defaultheight)

})

test('should be able to set the global shortcut keys', async () => {
  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()

  await page.getByText('×').nth(1).click()
  await page.getByRole('combobox', { name: 'Select box' }).click()
  await page.getByText('G', { exact: true }).click()
  expect(page.locator('form[name="settings"] div').filter({ hasText: 'Cmd' }).filter({ hasText: 'Ctrl'}).nth(2)).toBeVisible()
  expect(page.locator('form[name="settings"] div').filter({ hasText: 'G'}).nth(2)).toBeVisible()

  await Helper.fillDummyServer(page)
  await page.locator('#save-button').click()

  page = await Helper.waitForReload(electronApp)

  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()
  expect(page.locator('form[name="settings"] div').filter({ hasText: 'Cmd' }).filter({ hasText: 'Ctrl'}).nth(2)).toBeVisible()
  expect(page.locator('form[name="settings"] div').filter({ hasText: 'G'}).nth(2)).toBeVisible()
})

test('should be able to close in tray', async () => {
  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)
  await expect(page.locator('.form-settings')).toBeVisible()

  await page.waitForSelector('#optionCloseTray')
  await page.locator('#optionCloseTray').click()
  await Helper.fillDummyServer(page)
  await page.locator('#save-button').click()

  page = await Helper.waitForReload(electronApp)

  await expect(page.locator('.app-close')).toBeVisible()
  await Helper.clickAndWait(page, '.app-close', Helper.closeDelay);
  Helper.expectElectronInstancesNb(electronApp, 1)

  await ipcMainEmit(electronApp, 'focusBrowserWindow')

  const isMinimized: boolean = await electronApp.evaluate(async ({BrowserWindow}, delay) => {
    let mainWindow =  BrowserWindow.getAllWindows()[0]
    return new Promise(async (resolve) => {
      setTimeout(()=>{resolve(mainWindow.isMinimized())}, delay)
    })
  }, Helper.evalDelay);

  expect(isMinimized).toEqual(false)

})

test('should be launched with custom ini file', async () => {
  await electronApp.close()
  await Helper.delay(Helper.testDelay)
  electronApp = await Helper.genericBeforeEach()

  page = await electronApp.firstWindow()
  await expect(page.locator('.title-bar')).toBeVisible()
  Helper.clickAndWait(page, '.app-conf', Helper.evalDelay)

  await page.getByRole('combobox', { name: 'Application server' }).selectOption('0');
  await expect(page.locator('#altServers').filter({ hasText : 'dev'})).toBeVisible();
  await expect(page.locator('#optionCloseTray')).toHaveClass('ng-pristine ng-untouched ng-valid ng-not-empty')
  await expect(page.locator('#optionStartUp')).toHaveClass('ng-pristine ng-untouched ng-valid ng-empty')
})
