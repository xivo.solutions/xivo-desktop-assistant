import { expect, test } from '@playwright/test'
import { ElectronApplication, Page, _electron as electron } from '@playwright/test'
import Helper from '../helpers/playwright-helper';

let electronApp: ElectronApplication

test.beforeEach(async () => {
  await Helper.delay(Helper.testDelay)
  electronApp = await Helper.genericBeforeEach(["--ignore-certificate-errors=true", "--some-random-flag=foobarbat"])
})

test.afterEach(async () => { await Helper.genericAfterEach(electronApp) })

let page: Page
let sizeObject = Helper.getSizeObject()

test('should render the angular wrapper with the header bar', async () => {
  page = await electronApp.firstWindow()
  await page.content()
  await page.waitForSelector('ui-view')
  await expect(page.locator('.title-bar')).toBeVisible()
})

test('should start with the default size of the UC Assistant', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  const bounds = await page.evaluate(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  });

  expect(bounds.width).toEqual(sizeObject.application.ucassistant.defaultwidth)
  expect(bounds.height).toEqual(sizeObject.application.ucassistant.defaultheight)
})

test('should not allow a second instance to start and re-open the main one instead', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  await expect(page.locator('.title-bar')).toBeVisible()
  await Helper.clickAndWait(page, '.app-min', Helper.evalDelay)

  let infos: { minimized: boolean, focused: boolean } = await electronApp.evaluate(async ({ BrowserWindow }, delay) => {
    let mainWindow = BrowserWindow.getAllWindows()[0]
    return new Promise((resolve) => {
      setTimeout(() => { resolve({ minimized: mainWindow.isMinimized(), focused: mainWindow.isFocused() }) }, delay)
    })
  }, Helper.evalDelay)

  expect(infos.minimized).toEqual(true)

  await electron.launch({ args: ["."] })

  await expect(page.locator('.title-bar')).toBeVisible()

  infos = await electronApp.evaluate(async ({ BrowserWindow }, delay) => {
    let mainWindow = BrowserWindow.getAllWindows()[0]
    return new Promise(async (resolve) => {
      setTimeout(() => { resolve({ minimized: mainWindow.isMinimized(), focused: mainWindow.isFocused() }) }, delay)
    })
  }, Helper.evalDelay);

  Helper.expectElectronInstancesNb(electronApp, 1)

  expect(infos.minimized).toEqual(false)

})

test('should accept start flags', async () => {
  page = await electronApp.firstWindow()
  await page.content()

  let ignoreCertErrFlag = await electronApp.evaluate(async ({ app }) => {
    return new Promise(async (resolve) => {
      resolve(app.commandLine.getSwitchValue("ignore-certificate-errors"))
    })
  })

  let fooBarBatFlag = await electronApp.evaluate(async ({ app }) => {
    return new Promise(async (resolve) => {
      resolve(app.commandLine.getSwitchValue("some-random-flag"))
    })
  })

  expect(ignoreCertErrFlag).toEqual("true")
  expect(fooBarBatFlag).toEqual("foobarbat")

})
