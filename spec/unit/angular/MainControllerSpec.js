"use strict";

require("ts/renderer/ApplicationAngular");


describe("MainController", function() {
  var $scope;

  beforeAll(() => {
    spyOn(console, 'log');
  })

  beforeEach(ngModule('desktopApp'));

  var $controller;
  beforeEach(inject(function(_$controller_, $injector, _updateService_, _$rootScope_) {
    $controller = _$controller_;
    $scope = _$rootScope_.$new();
    spyOn(_updateService_, 'setUpdateShortcut');
    spyOn(_updateService_, 'setUpdateCloseTray');
    spyOn(_updateService_, 'setUpdateStartUp');
    spyOn(_updateService_, 'reloadApp');
    spyOn(_updateService_, 'setUpdateUrl');
  }));

  beforeEach(inject(function() {
    var html = '<webview src="localhost"></webview>';
    angular.element(document.body).append(html);
  }));

  it("can be started", inject(function() {
    $scope = {};
    var ctrl = $controller("MainController", {'$scope':$scope});
    expect(ctrl).toBeDefined();
  }));

  it("redirect to settings page if remote url is not set", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.returnValue(null);
    spyOn(_$state_, "go");
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_, $state: _$state_});
    expect(_$state_.go).toHaveBeenCalledWith('main.settings');
  }));

  it("set url if remote url is set", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_PROTOCOL': return 'http';
      case 'APP_DOMAIN': return 'go.to.somewhere';
      case 'APP_INTERFACE': return 'ucassistant';
      case 'APP_TOKEN': return '';
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    var updateUrl = "http://go.to.somewhere";
    expect($scope.url).toBe('http://go.to.somewhere/ucassistant');
    expect(_updateService_.reloadApp).not.toHaveBeenCalled();
  }));

  it("set shortcut to default if not defined", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return null;
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith(_desktopSettings_.DEFAULT_SHORTCUT);
  }));

  it("set shortcut to what have been saved", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "Ctrl-X";
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith("Ctrl-X");
  }));

  it("set shortcut to null if disabled", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "null";
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith('null');
  }));

  it("Error should be throwed webview failed to load", inject((_updateService_, _$state_) => {
    const $scope = {};
    spyOn(_updateService_, "setError");
    spyOn(_updateService_, "throwUrlError");
    $controller("MainController", {$scope:$scope});
    $scope.throwWebviewError();
    expect(_updateService_.setError).toHaveBeenCalled();
  }));

  it("Error should be cleaned after webview success to load", inject((_updateService_, _$state_) => {
    const $scope = {};
    spyOn(_updateService_, "clearError");
    $controller("MainController", {$scope:$scope});
    $scope.successWebviewLoad();
    expect(_updateService_.clearError).toHaveBeenCalled();
  }));

  it("show/hide phonebar on click when jitsi iframe", () => {
    const $scope = {};
    $controller("MainController", {$scope:$scope});
    $scope.isToggled = false;
    $scope.togglePhoneApp();
    expect($scope.isToggled).toBeTruthy();
    $scope.togglePhoneApp();
    expect($scope.isToggled).toBeFalsy();
  });

  it("set token in localstorage and reload url", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });
    const token = 'aaaa-bbbb-cccc-dddd';

    $scope.handleToken(token);
    expect(_updateService_.reloadApp).toHaveBeenCalled();
    expect(_desktopSettings_.get("APP_TOKEN")).toEqual(token);
  }))

  it("set empty in localstorage and reload url without token", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });

    $scope.handleToken("");
    expect(_updateService_.reloadApp).toHaveBeenCalled();
    expect(_desktopSettings_.get("APP_TOKEN")).toEqual("");
  }))

  it("replace old token with new and reload url", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};
    spyOn(_desktopSettings_, "set");
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch (key) {
        case 'APP_TOKEN': return 'qwerty';
      }
      return null;
    });

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });
    var newToken = "azerty";

    $scope.handleToken(newToken);

    expect(_desktopSettings_.set).toHaveBeenCalledWith("APP_TOKEN", newToken);
    expect(_updateService_.reloadApp).toHaveBeenCalled();
  }))

  it("doesnt reload if the same token is passed", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};
    var sameToken = "qwerty";
    spyOn(_desktopSettings_, "set");
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch (key) {
        case 'APP_TOKEN': return sameToken;
      }
      return null;
    });

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });

    $scope.handleToken(sameToken);

    expect(_desktopSettings_.set).not.toHaveBeenCalled();
    expect(_updateService_.setUpdateUrl).not.toHaveBeenCalled();
    expect(_updateService_.reloadApp).not.toHaveBeenCalled();
  }))

  it("should not reload url if token is null", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};
    var newToken = null;
    spyOn(_desktopSettings_, "set");
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch (key) {
        case 'APP_TOKEN': return newToken;
      }
      return null;
    });

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });

    $scope.handleToken(newToken);

    expect(_desktopSettings_.set).not.toHaveBeenCalled();
    expect(_updateService_.setUpdateUrl).not.toHaveBeenCalled();
    expect(_updateService_.reloadApp).not.toHaveBeenCalled();
  }))

  it("should not reload url if token is empty", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};
    var newToken = "";
    spyOn(_desktopSettings_, "set");
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch (key) {
        case 'APP_TOKEN': return newToken;
      }
      return null;
    });

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });

    $scope.handleToken(newToken);

    expect(_desktopSettings_.set).not.toHaveBeenCalled();
    expect(_updateService_.setUpdateUrl).not.toHaveBeenCalled();
    expect(_updateService_.reloadApp).not.toHaveBeenCalled();
  }))

  it("should not reload url if token is undefined", inject((_updateService_, _desktopSettings_) => {
    const $scope = {};
    var newToken = undefined;
    spyOn(_desktopSettings_, "set");
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch (key) {
        case 'APP_TOKEN': return newToken;
      }
      return null;
    });

    var ctrl = $controller("MainController", { $scope: $scope, desktopSettings: _desktopSettings_, updateService: _updateService_ });

    $scope.handleToken(newToken);

    expect(_desktopSettings_.set).not.toHaveBeenCalled();
    expect(_updateService_.setUpdateUrl).not.toHaveBeenCalled();
    expect(_updateService_.reloadApp).not.toHaveBeenCalled();
  }))
});
