"use strict";

require("ts/renderer/ApplicationAngular");

describe("DesktopSettings service", function () {
  var _realLocalStorage;

  beforeAll(() => {
    _realLocalStorage = localStorage;
    spyOn(console, 'log');
  });

 beforeEach(ngModule('desktopApp'));

  beforeEach(() => {
    var items = {};
    localStorage = {
      getItem: function (k) { return items[k]; },
      setItem: function (k, v) { items[k] = v; },
      clear: function () { console.log(items.length = 0); }
    }
  });

  afterAll(() => {
    localStorage = _realLocalStorage;
  });

  it('should get ucassistant as interface default value', inject(function (_desktopSettings_) {
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get the localstorage interface key value when set to ucassistant', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_UCASSISTANT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get the localstorage interface key value when set to ccagent', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_CCAGENT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should get ccagent as interface if it is set in server', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/ccagent");
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should get ccagent as interface if it is set in server', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "http");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/ccagent");
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should get the localstorage interface key value when set to switchboard', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_SWITCHBOARD);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_SWITCHBOARD);
  }));

  it('should get switchboard as interface if it is set in server', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/switchboard");
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_SWITCHBOARD);
  }));

  it('should get switchboard as interface if it is set in server', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "http");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/switchboard");
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_SWITCHBOARD);
  }));

  it('should ignore server interface and get switchboard interface key if it is set', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/ucassistant");
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_SWITCHBOARD);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_SWITCHBOARD);
}));

  it('should get ucassistant as interface if it is set in server and interface key is empty', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "http");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/ucassistant");
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get ucassistant as interface if no interface is set in server and interface key is empty', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "http");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com");
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should ignore server interface and get ucassistant interface key if it is set', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/ccagent");
      localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_UCASSISTANT);
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should ignore server interface and get ccagent interface key if it is set', inject(function (_desktopSettings_) {
      localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
      localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com/ucassistant");
      localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_CCAGENT);
      expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should extract the last part of the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server1)).toEqual('ucassistant');

    let server2 = "http://www.azerty.com/ccagent";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server2)).toEqual('ccagent');

    let server3 = "http://www.azerty.com/";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server3)).toEqual(undefined);

    let server4 = "http://www.azerty.com";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server4)).toEqual(undefined);

    let server5 = "http://www.azerty.com/uiop/ccagent";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server5)).toEqual('ccagent');

    let server6 = "http://www.azerty.com/uiop/";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server6)).toEqual(undefined);

    let server7 = "http://www.azerty.com/uiop/qsdfg";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server7)).toEqual(undefined);
  }));

  it('should get the server update url', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server1)).toEqual('http://www.azerty.com');

    let server2 = "http://www.azerty.com/ccagent";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server2)).toEqual('http://www.azerty.com');

    let server3 = "http://www.azerty.com/";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server3)).toEqual('http://www.azerty.com');

    let server4 = "http://www.azerty.com";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server4)).toEqual('http://www.azerty.com');

    let server5 = "http://www.azerty.com/uiop/ccagent";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server5)).toEqual('http://www.azerty.com/uiop');

    let server6 = "http://www.azerty.com/uiop/";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server6)).toEqual('http://www.azerty.com/uiop');

    let server7 = "http://www.azerty.com/uiop/qsdfg";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server7)).toEqual('http://www.azerty.com/uiop/qsdfg');

  }));

  it('should take off the protocol from the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server1)).toEqual('www.azerty.com/ucassistant');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server2)).toEqual('www.azerty.com/ucassistant');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server3)).toEqual('www.azerty.com');

    let server4 = "myhttpserver";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server4)).toEqual('myhttpserver');

    let server5 = "myhttp://serverhttps://";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server5)).toEqual('myhttp://serverhttps://');

  }));

  it('should return the protocol from the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressProtocol(server1)).toEqual('http');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressProtocol(server2)).toEqual('https');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressProtocol(server3)).toEqual('https');

  }));

  it('should return the server address without protocol or interface', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressOnly(server1)).toEqual('www.azerty.com');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressOnly(server2)).toEqual('www.azerty.com');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressOnly(server3)).toEqual('www.azerty.com');

  }));

  it('should see if local storage is different than ini file', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com");

    let data = [
      ["APP_PROTOCOL", "http"],
      ["APP_DOMAIN", "test.fr"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(true)
  }))

  it('should see if ini file have same config as local storage', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com");

    let data = [
      ["APP_PROTOCOL", "https"],
      ["APP_DOMAIN", "www.azerty.com"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(false)
  }))

  it('should see if local storage is different than ini file bool', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_STARTUP, false);
    localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com");

    let data = [
      ["APP_PROTOCOL", "https"],
      ["APP_DOMAIN", "www.azerty.com"],
      ["APP_STARTUP", true]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(true)
  }))

  it('should see if ini file have same config as local storage bool', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_PROTOCOL, "https");
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "www.azerty.com");
    localStorage.setItem(_desktopSettings_.KEY_APP_STARTUP, true);

    let data = [
      ["APP_PROTOCOL", "https"],
      ["APP_DOMAIN", "www.azerty.com"],
      ["APP_STARTUP", true]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(false)
  }))

  it('should compare correctly even if localstorage is empty', inject(function (_desktopSettings_) {
    let data = [
      ["APP_PROTOCOL", "https"],
      ["APP_DOMAIN", "azerty.fr"],
      ["ALT_SERVERS", "{'aze':'azerty'}"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(true)
  }))

  it('should compare correctly if alt servers are the same', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_ALT_SERVERS, "{'aze':'azerty','dev':'dev.dev'}");

    let data = [
      ["ALT_SERVERS", "{'aze':'azerty','dev':'dev.dev'}"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(false)
  }))

  it('should compare correctly if alt servers are different', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_ALT_SERVERS, "{'aze':'azerty','test':'test.test'}");

    let data = [
      ["ALT_SERVERS", "{'aze':'azerty','dev':'dev.dev'}"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(true)
  }))

  it('should compare correctly string value', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "test.fr");

    let data = [
      ["APP_DOMAIN", "test.fr"]
    ]

    let data2 = [
      ["APP_DOMAIN", "undefined.def"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(false)
    expect(_desktopSettings_.hasDifferencesInLocalStorage(data2)).toEqual(true)

  }))

  it('should compare correctly boolean value', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_STARTUP, "true");
    localStorage.setItem(_desktopSettings_.KEY_APP_CLOSE, "true");

    let data = [
      ["APP_STARTUP", "true"]
    ]

    let data2 = [
      ["APP_CLOSE", "false"]
    ]

    expect(_desktopSettings_.hasDifferencesInLocalStorage(data)).toEqual(false)
    expect(_desktopSettings_.hasDifferencesInLocalStorage(data2)).toEqual(true)
  }))

});
