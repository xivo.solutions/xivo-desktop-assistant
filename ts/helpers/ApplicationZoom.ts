import SizeMgt from '../main/ApplicationSizeManager'

export default class Zoom {
    private readonly MAX_ZOOM_FACTOR: number = 4.9;
    private readonly MIN_ZOOM_FACTOR: number = 0.2;

	constructor(private win: Electron.BrowserWindow, private sizeMgt: SizeMgt) {}
  
	public zoomIn(): void {
	  if (this.win.webContents) {
		const currentZoomFactor = this.win.webContents.getZoomFactor()
	    const newZoomFactor = Number((currentZoomFactor + 0.1).toFixed(1))
		if (newZoomFactor <= this.MAX_ZOOM_FACTOR) {
			this.win.webContents.setZoomFactor(newZoomFactor)
		}
	  }
	}
  
	public zoomOut(): void {
	  if (this.win.webContents) {
		const currentZoomFactor = this.win.webContents.getZoomFactor()
		const newZoomFactor = Number((currentZoomFactor - 0.1).toFixed(1))
		if (newZoomFactor >= this.MIN_ZOOM_FACTOR) {
			this.win.webContents.setZoomFactor(newZoomFactor)
		}
	  }
	}
  
	public zoomReset(): void {
	  if (this.win.webContents) {
		this.win.webContents.setZoomFactor(1);
		this.sizeMgt.resetWindowSize(this.win)
	  }
	}
}
  