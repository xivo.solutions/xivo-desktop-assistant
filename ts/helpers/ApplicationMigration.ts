const { app } = require('electron')
const fs = require('fs')
const { COPYFILE_EXCL } = fs.constants
const log = require('electron-log')
const path = require('path')

var newConfigPath: string = app.getPath('appData') + '/xivo-desktop-assistant/application/config'
var newConfigFile: string = newConfigPath + '/xivoconfig.ini'
var oldConfigFile: string = './xivoconfig.ini'
var oldSubConfigFolder: string = '../config'
var oldSubConfigFile: string = path.join(oldSubConfigFolder, oldConfigFile)

export default class Migration {

  public static checkForOldConfig = () => {
    return new Promise<void>((resolve): void => {
      if (fs.existsSync(oldConfigFile) && !fs.existsSync(newConfigFile)) {
        fs.copyFileSync(oldConfigFile, newConfigFile, COPYFILE_EXCL)
        fs.unlinkSync(oldConfigFile)
        log.info('Copied old xivoconfig file to : ' + newConfigPath)
      }
      resolve()
    }).catch((err: Error) => {
      log.error(err)
      return err
    })
  }

  public static checkForOldSubConfig = () => {
    return new Promise<void>((resolve) => {
      if (fs.existsSync(oldSubConfigFile) && !fs.existsSync(newConfigFile)) {
        fs.copyFileSync(oldSubConfigFile, newConfigFile, COPYFILE_EXCL)
        fs.unlinkSync(oldSubConfigFile)
        fs.rmdirSync(oldSubConfigFolder)
        log.info('Copied old xivoconfig file to : ' + newConfigPath)
      }
      resolve()
    }).catch((err: Error) => {
      log.error(err)
      return err
    })
  }


  public static migrate = (): Promise<void> => {
    try {
      return new Promise<void>((resolve): void => {
        if (!fs.existsSync(newConfigFile)) {
          Migration.checkForOldConfig().then(() => {
            Migration.checkForOldSubConfig().then(() => {
              resolve()
            })
          })
        } else { resolve() }
      })
    } catch (err) {
      log.error(err)
      return Promise.reject()
    }
  }
}
