const { app, dialog, globalShortcut } = require('electron')
const log = require('electron-log')
const QUIT_DELAY = 3000;

import Message from './Message'

export default class MenuTools {

  private readonly confirmQuitEvent: string = "CONFIRM_QUIT_EVENT"

  constructor(
    protected win: Electron.BrowserWindow,
    protected closeParams: XiVO.Options,
    protected tray: Electron.Tray
   ) {}

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  protected forceQuit(): void {
    log.info("Received event to close app")
    if(this.win.isVisible()) this.win.hide()
    globalShortcut.unregisterAll()
    if (this.tray) this.tray.destroy()
    this.delay(QUIT_DELAY).finally(() => app.exit())
  }

  protected forwardMessage(_: Electron.BrowserWindow, msg: string, type: string): void {
    try {
      if (msg) {
        let data = JSON.stringify(new Message(type, msg))
        this.win.webContents.send('msg', data)
      }
    }
    catch (exception) {
      log.error(exception)
    }
  }

  protected closeApp(event?: Event): void {
    if (this.closeParams.quit) {
      this.forceQuit()
    } else {
      if (this.closeParams.closeInTray && event) {
        event.preventDefault()
        this.win.hide()
      } else {
        this.confirmQuit()
      }
    }
  }

  protected confirmQuit = (): void => {
    if (this.closeParams.confirm) {
      let messageBoxAppeared = dialog.showMessageBox(this.win, <any>this.closeParams.confirm)
      messageBoxAppeared.then((mbrv: Electron.MessageBoxReturnValue) => {
        if (mbrv.response > 0) this.forwardMessage(this.win, 'confirmQuit', this.confirmQuitEvent)
      }).catch((err: Error) => {
        log.error(err)
      })
    } else {
      this.forwardMessage(this.win, 'confirmQuit', this.confirmQuitEvent)
      this.forceQuit()
    }
  }

  protected setFocus(browserWindow: Electron.BrowserWindow): void {
    if (browserWindow) {
      browserWindow.restore()
      browserWindow.show()
      browserWindow.focus()
    }
  }
}