var i18n = require('i18n')

i18n.configure({
  locales:['en', 'fr', 'de'],
  directory: __dirname + '/../../locales',

  logWarnFn: (msg: string): void => {
    console.log('warn', msg);
  },

  logErrorFn: (msg: string): void => {
    console.log('error', msg);
  }
})

module.exports = i18n
