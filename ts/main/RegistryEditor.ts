const fs = require('fs')
const path = require('path')
const Winreg = require('winreg')
const log = require('electron-log')

import WinReg from 'winreg'

export default class RegistryEditor {

  constructor(
    private appname: string,
    private app: any
  ) {}

  public overrideUpdaterPath(): void {
    let runRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\Microsoft\\Windows\\CurrentVersion\\Run'})
    runRegistry.get('xivo-desktop-assistant',(_: Error, updaterPath: WinReg.RegistryItem) => {
      if (!updaterPath || (updaterPath.value.length > 0 && updaterPath.value.includes("update.exe"))) {
        let exePath = path.join(path.dirname(process.execPath), '..', 'xivo-desktop-assistant.exe')
        if (fs.existsSync(exePath)) {
          runRegistry.set(this.appname, Winreg.REG_SZ, "\"" + exePath + "\"", (err: Error) => {log.error(err)})
          log.info("Updater set to correct exe")
        }
      }
    })
  }

  public setProtocols(protocols: Array<string>): void {
    if (process.platform === 'win32') {
        let capabilitiesRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\'+this.appname+'\\Capabilities'})
        let registeredApplicationRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\RegisteredApplications'})

        capabilitiesRegistry.set('ApplicationName', Winreg.REG_SZ, this.appname, (_err: Error) => {})
        capabilitiesRegistry.set('ApplicationDescription', Winreg.REG_SZ, this.appname, (_err: Error) => {})
        registeredApplicationRegistry.set(this.appname, Winreg.REG_SZ, 'Software\\'+this.appname+'\\Capabilities', (_err: Error) => {})

        for (let p of protocols) {
          let URLAssociationRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\'+this.appname+'\\Capabilities\\URLAssociations'})
          URLAssociationRegistry.set(p, Winreg.REG_SZ, this.appname+'.'+p, (_err: Error) => {})

          let DefaultIconRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\Classes\\'+this.appname+'.'+p+'\\DefaultIcon'})
          DefaultIconRegistry.set('', Winreg.REG_SZ, process.execPath, (_err: Error) => {})

          let shellCommandRegistry: WinReg.Registry = new Winreg({hive: Winreg.HKCU, key: '\\Software\\Classes\\'+this.appname+'.'+p+'\\shell\\open\\command'})
          shellCommandRegistry.set('', Winreg.REG_SZ, `"${process.execPath}" "%1"`, (_err: Error) => {})        }

    } else {
      protocols.forEach((p) => {
        this.app.setAsDefaultProtocolClient(p)
      })
    }
  }

}
