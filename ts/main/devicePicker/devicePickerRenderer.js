const HIDDeviceList = document.getElementById("device-list");

let HIDDevices = [];

function createItemBodyContent(text) {
  const div = document.createElement('div');
  div.className = 'items-body-content';

  const span = document.createElement('span');
  span.textContent = text;

  div.appendChild(span);

  return div;
}

function devicePicked(device) {
   window.api.send("devicePicked", device.id);
}

function close() {
  window.api.send("devicePickerClosed");
}

window.api.receive("devices", (data) => {

  HIDDevices = [...data];

  setTimeout(() => {
    document.getElementById('loader').remove()
    if (HIDDevices && HIDDevices.length > 0) {
      HIDDevices.forEach((device, index) => {

        let item = createItemBodyContent(device.name);
        item.addEventListener("click", function () {devicePicked(this);});
        item.id = HIDDevices[index].deviceId;

        HIDDeviceList.appendChild(item);
      })
    } else {
      const div = document.createElement('div');
      div.textContent = translate("no-device", navigator.language);
      div.id = "no-device"
      HIDDeviceList.appendChild(div);
    }
  }, 500)

});

let closeButtton = document.getElementById("close");
closeButtton.onclick = close;
document.getElementById("header-text").textContent = translate("header-text", navigator.language)
document.getElementById('loading-mask').remove()
window.api.send("pickingReady");

function translate(str, lang) {
  switch (str) {
    case "header-text":
      return lang == "fr-FR" ? "Sélectionnez votre périphérique" : lang == "de-DE" ? "Wählen Sie Ihr Gerät aus" :	"Select your device";
    case "no-device":
      return lang == "fr-FR" ? "Aucun périphérique détecté" : lang == "de-DE" ? "Kein Gerät gefunden" : "No device found";
  }
}