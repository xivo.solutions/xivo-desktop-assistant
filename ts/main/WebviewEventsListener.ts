const { app, shell, ipcMain} = require('electron')

export default class WebviewEventsListener {

    public static webview: any = undefined

    constructor() {}

    public initWebviewListeners(): void {
            app.on('web-contents-created', (_, contents: Electron.WebContents): void => {                
            if (contents.getType() === 'webview') {
                WebviewEventsListener.webview = contents
                contents.setWindowOpenHandler(({ url }) => {
                    console.log(`Opening URL ${url} in default browser`)
                    shell.openExternal(url);
                    return { action: 'deny' };
                  });
            }
        })

        ipcMain.on('webviewLoadFail', () => WebviewEventsListener.webview = undefined)
    }
}