const i18n = require('../helpers/translate')
const path = require('path')
const {app} = require('electron')
const { Menu } = require('electron')
const ctxMenu = require('electron-context-menu')
const openAboutWindow = require('about-window').default
import MenuTools from '../helpers/ApplicationMenuTools'
import WebviewEventsListener from '../main/WebviewEventsListener'
import Zoom from '../helpers/ApplicationZoom'


export default class ApplicationMenu extends MenuTools {
  public contextMenu: Electron.Menu

  constructor(
    protected win: Electron.BrowserWindow,
    protected closeParams: XiVO.Options,
    protected tray: Electron.Tray,
    protected zoom: Zoom

  ) { super(win, closeParams, tray)
      const applicationMenu = this
      this.contextMenu = Menu.buildFromTemplate([
        {
        label: i18n.__('MENU_NAVIGATION_SHOW'),
        click() {
          applicationMenu.setFocus(win)
        }
        },
        {
        label: i18n.__('MENU_NAVIGATION_SETTINGS'),
        click() {
        win.webContents.send('sref', 'main.settings')
        applicationMenu.setFocus(win)
        }
        },
        {
        type: 'separator'
        },
        {
        label: i18n.__('MENU_NAVIGATION_QUIT'),
        click() {
          applicationMenu.closeApp()
        }
        }])
  }

  public rightClickEventListener(): void {
    const applicationMenu = this


    ctxMenu({
      showSearchWithGoogle: false,
      showLookUpSelection: false,
      showInspectElement: false,

        prepend: () => [
        {
          label: i18n.__('MENU_NAVIGATION'),
          submenu: [
            {
              label: i18n.__('MENU_NAVIGATION_APPLICATION'),
              click() {
                applicationMenu.win.webContents.send('sref', 'main')
              }
            },
            {
              label: i18n.__('MENU_NAVIGATION_SETTINGS'),
              click() {
                applicationMenu.win.webContents.send('sref', 'main.settings')
              }
            },
            {
              type: 'separator'
            },
            {
              label: i18n.__('MENU_DEBUG_RELOAD'),
              accelerator: 'CmdOrCtrl+R',
              click() {
                applicationMenu.win.reload()
              }
            },
            {
              label: i18n.__('MENU_NAVIGATION_QUIT'),
              click() {
                applicationMenu.closeApp()
              }
            },
            {
              type: 'separator'
            },
            {
              label: i18n.__('MENU_NAVIGATION_ABOUT'),
              click() {
                openAboutWindow({
                  icon_path: path.join(__dirname, '/../../img/xivo_default.png'),
                  copyright: i18n.__('LICENSING'),
                  win_options: {
                    autoHideMenuBar: true
                  }
                })
              }
            }
          ]
        },
        {
          label: i18n.__('MENU_DEBUG'),
          submenu: [
            {
              label: i18n.__('MENU_DEBUG_DEVTOOL'),
              click() {
                applicationMenu.win.webContents.toggleDevTools()
              }
            },
            {
              label: i18n.__('MENU_DEBUG_DEVTOOL_WEBVIEW'),
              click() {
                if (WebviewEventsListener.webview) {
                  WebviewEventsListener.webview.toggleDevTools()
                }
              }
            }
          ]
        },
        {
          label: i18n.__('MENU_DEBUG_ZOOMRESET'),
          click() {
            applicationMenu.zoom.zoomReset()
          }
        }
      ]
    });

    app.on("web-contents-created", (_, contents: any): void => {
      ctxMenu({
        window: contents,
        showSearchWithGoogle: false,
        showInspectElement: false,
      });
    })

    applicationMenu.win.webContents.on("will-attach-webview", (_, params: Electron.WebPreferences): void => {
      params.spellcheck = false
      })
  }
};
