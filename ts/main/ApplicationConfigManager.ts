const { app } = require('electron')
const configFolderPath: string = app.getPath('appData') + '/xivo-desktop-assistant/application/config'
const configFilePath: string = configFolderPath + '/xivoconfig.ini'

const iniFileTemplate = `
;Defines if the connection is secured or not : HTTPS or HTTP
APP_PROTOCOL=

;Defines the server used by the application : IP or URL
APP_DOMAIN=

;Defines the interface to open by default : UCASSISTANT or CCAGENT
APP_INTERFACE=

;Defines if the UC assistant opens on operating system startup : true or false
APP_STARTUP=

;Defines if the UC assistant will be minimized in the taskbar instead of closed : true or false
APP_CLOSE=

;List of servers (override APP_DOMAIN) : SERVER DISPLAY NAME = SERVER ADDRESS, where server does not include the protocol, one pair per line
[ALT_SERVERS]
`

const fs = require('fs')
const ini = require('ini')
const log = require('electron-log')

import Migration from '../helpers/ApplicationMigration'

export default class ConfigManager {
  private iniConfig: Array<[string,string]>
  private rendererConfigured: boolean = false

  constructor(){
    this.iniConfig = []
  }

  public initApplicationConfigFile = (): void => {
    const iniFileAsync = ConfigManager.getXivoIniFile()
    iniFileAsync
      .then((config: XiVO.Config) => {
        for (let setting in config) {
          let value = config[setting]
          if (typeof value == 'boolean' || value) {
            if (setting == 'ALT_SERVERS') {
              value = JSON.stringify(value)
            } else if (typeof value == 'string') {
              value = value.toLowerCase()
            }
          this.iniConfig.push([setting, value])
          }
        }
      }).catch((err: Error) => {
        log.error(err)
      })
  }

  public applyConfiguration(win: Electron.BrowserWindow): void {
    if (this.iniConfig.length > 0 && !this.rendererConfigured) {
      this.initApplicationConfigFile()
      log.info(`Sending ${this.iniConfig} configuration to webcontent`)
      win.webContents.send('handleLocalStorageConfig', this.iniConfig)
      this.rendererConfigured = true
    }
  }

  public static getXivoIniFile(): Promise<any> {

  let createDefaultConfigFile = (path: string, resolve: (value?: any) => void, reject: (err: Error) => void) => {
    fs.appendFile(path, iniFileTemplate, (err: Error) => {
        err ? reject(err) : resolve()
    })
  }

  return new Promise((resolveReturn, rejectReturn): void => {

    let folderIsReady = () => {
      return new Promise<void>((resolve: (value?: any) => void): void => {
        if (!fs.existsSync(configFolderPath)) {
          fs.mkdirSync(configFolderPath, { recursive: true })
        }
        resolve()
      }).catch((err: Error): Error => {
        log.error(err)
        return err
      })
    }

    let configIsReady = () => {
      return new Promise<void>((resolve, reject): void => {
        if (!fs.existsSync(configFilePath)) {
          createDefaultConfigFile(configFilePath, resolve, reject)
        } else {
          resolve()
        }
      }).catch((err: Error) => {
        log.error(err)
        return err
      })
    }

    try {
      folderIsReady().then(() => {
        Migration.migrate().then(() => {
          configIsReady().then(() => {
            fs.readFile(configFilePath, (err: Error, data: JSON) => {
              if (err) rejectReturn('Ini file cannot be read (' + err + ')')
              log.info('Ini file has been succesfully loaded')
              resolveReturn(ini.parse(data.toString()))
            })
          })
        })
      })
    } catch (err) {
      rejectReturn('Ini file cannot be read (' + err + ')')
    }

  })
  }
}
