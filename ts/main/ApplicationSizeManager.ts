import { BrowserWindow, screen } from "electron"

export default class SizeMgt {

  public readonly UCASSISTANT_APPINTERFACE: string = 'ucassistant'
  private readonly UCASSISTANT_DEFAULT_WIDTH: number = 470
  private readonly UCASSISTANT_DEFAULT_HEIGHT: number = 774

  public readonly CCAGENT_APPINTERFACE: string = 'ccagent'
  private readonly CCAGENT_DEFAULT_WIDTH: number = 362
  private readonly CCAGENT_DEFAULT_HEIGHT: number = 824

  public readonly CCAGENT_MINIBAR_APPINTERFACE: string = 'minibar'
  private readonly CCAGENT_MINIBAR_WIDTH = 72;
  private readonly CCAGENT_MINIBAR_HEIGHT = 699;
  
  public readonly SWITCHBOARD_APPINTERFACE: string = 'switchboard'
  private readonly SWITCHBOARD_DEFAULT_WIDTH: number = 720
  private readonly SWITCHBOARD_DEFAULT_HEIGHT: number = 824

  private readonly MINIMUM_WIDTH_WHILE_IN_MEETING_ROOM = 800
  private readonly   MINIMUM_WIDTH_WHILE_SWITCHBOARD_IN_MEETING_ROOM = 1000

  private appInterface: string = this.UCASSISTANT_APPINTERFACE
  public previousSize: XiVO.WindowSize = { width: this.UCASSISTANT_DEFAULT_WIDTH, height: this.UCASSISTANT_DEFAULT_HEIGHT }

  constructor() {}

  private setLocalStorageDefaults(application: string):void {
    switch (application) {
      case this.UCASSISTANT_APPINTERFACE:
        this.appInterface = this.UCASSISTANT_APPINTERFACE
        this.previousSize = { width: this.UCASSISTANT_DEFAULT_WIDTH, height: this.UCASSISTANT_DEFAULT_HEIGHT }
        break;
      case this.CCAGENT_APPINTERFACE:
        this.appInterface = this.CCAGENT_APPINTERFACE
        this.previousSize = { width: this.CCAGENT_DEFAULT_WIDTH, height: this.CCAGENT_DEFAULT_HEIGHT }
        break;
      case this.CCAGENT_MINIBAR_APPINTERFACE:
      this.appInterface = this.CCAGENT_MINIBAR_APPINTERFACE
      this.previousSize = { width: this.CCAGENT_MINIBAR_WIDTH, height: this.CCAGENT_MINIBAR_HEIGHT }
      break;
      case this.SWITCHBOARD_APPINTERFACE:
        this.appInterface = this.SWITCHBOARD_APPINTERFACE
        this.previousSize = { width: this.SWITCHBOARD_DEFAULT_WIDTH, height: this.SWITCHBOARD_DEFAULT_HEIGHT }
        break;
      default:
        this.appInterface = this.UCASSISTANT_APPINTERFACE
        this.previousSize = { width: this.UCASSISTANT_DEFAULT_WIDTH, height: this.UCASSISTANT_DEFAULT_HEIGHT }
        break;
    }
  }
  
  private getSizeOfWindow(win: BrowserWindow): XiVO.WindowSize {
    let size = win.getSize()
    return {width: size[0], height: size[1]}
  }

  private restoreWindowSize(win: BrowserWindow): void {
    this.updateSize(win, this.previousSize)
  }

  private updateSize(win: BrowserWindow, size: XiVO.WindowSize): void {
    let currentSize = win.getSize();
    
    if (currentSize[0] != size.width || currentSize[1] != size.height) {
      win.setSize(size.width, size.height)
    }
  }
  
  private saveWindowSize(win: BrowserWindow): void {
    this.previousSize = this.getSizeOfWindow(win)
  }

  private windowWasMaximized(win: BrowserWindow): boolean {
    let currentSize = this.getSizeOfWindow(win)
    return currentSize.width == this.previousSize.width && currentSize.height == this.previousSize.height
  }

  public resetWindowSize(win: BrowserWindow): void {
    let factor = Number((win.webContents.getZoomFactor()).toFixed(1))
    switch (this.appInterface) {
      case this.UCASSISTANT_APPINTERFACE:
        win.setMinimumSize(this.UCASSISTANT_DEFAULT_WIDTH, this.UCASSISTANT_DEFAULT_HEIGHT)
        this.updateSize(win, {width: Math.ceil(this.UCASSISTANT_DEFAULT_WIDTH * factor), height: Math.ceil(this.UCASSISTANT_DEFAULT_HEIGHT * factor)})
        break
      case this.CCAGENT_APPINTERFACE:
        win.setMinimumSize(this.CCAGENT_DEFAULT_WIDTH, this.CCAGENT_DEFAULT_HEIGHT)
        this.updateSize(win, {width: Math.ceil(this.CCAGENT_DEFAULT_WIDTH * factor), height: Math.ceil(this.CCAGENT_DEFAULT_HEIGHT * factor)})
        break
      case this.SWITCHBOARD_APPINTERFACE:
        win.setMinimumSize(this.SWITCHBOARD_DEFAULT_WIDTH, this.SWITCHBOARD_DEFAULT_HEIGHT)
        this.updateSize(win, {width: Math.ceil(this.SWITCHBOARD_DEFAULT_WIDTH * factor), height: Math.ceil(this.SWITCHBOARD_DEFAULT_HEIGHT * factor)})
        break
      case this.CCAGENT_MINIBAR_APPINTERFACE:
        win.setMinimumSize(this.CCAGENT_MINIBAR_WIDTH, this.CCAGENT_MINIBAR_HEIGHT)
        this.updateSize(win, {width: Math.ceil(this.CCAGENT_MINIBAR_WIDTH * factor), height: Math.ceil(this.CCAGENT_MINIBAR_HEIGHT * factor)})
        break
    }
    this.saveWindowSize(win)
  }

  public maximizeWindow(win: BrowserWindow, ): void {
    this.saveWindowSize(win)
    win.maximize()
  }

  public unmaximizeWindow(win: BrowserWindow, forceUnmaximize: boolean = false): void {
    if (forceUnmaximize || !this.windowWasMaximized(win)) {
      if (win.isMaximized()) {
        win.unmaximize()
        this.restoreWindowSize(win)
      } else win.unmaximize()
    }
  }

  public unmaximizeWhileInVideo(win: BrowserWindow): void {
    switch (this.appInterface) {
      case this.UCASSISTANT_APPINTERFACE:
        win.unmaximize()
        this.updateSize(win, {width: this.MINIMUM_WIDTH_WHILE_IN_MEETING_ROOM, height: this.UCASSISTANT_DEFAULT_HEIGHT})
        break
      case this.SWITCHBOARD_APPINTERFACE:
        win.unmaximize()
        this.updateSize(win, {width: this.MINIMUM_WIDTH_WHILE_SWITCHBOARD_IN_MEETING_ROOM, height: this.SWITCHBOARD_DEFAULT_HEIGHT})
        break
      case this.CCAGENT_APPINTERFACE:
        win.unmaximize()
        this.updateSize(win, {width: this.MINIMUM_WIDTH_WHILE_IN_MEETING_ROOM, height: this.CCAGENT_DEFAULT_HEIGHT})
        break
    }
  }  

  public updateAppInterface(win: BrowserWindow, arg: string): void {
      this.setLocalStorageDefaults(arg)
      this.resetWindowSize(win)
  }

  public getAppInterfaceSize(): XiVO.WindowSize {
    switch (this.appInterface) {
      case this.CCAGENT_APPINTERFACE:
        return {width: this.CCAGENT_DEFAULT_WIDTH, height: this.CCAGENT_DEFAULT_HEIGHT}
      case this.SWITCHBOARD_APPINTERFACE:
        return {width: this.SWITCHBOARD_DEFAULT_WIDTH, height: this.SWITCHBOARD_DEFAULT_HEIGHT}
      case this.UCASSISTANT_APPINTERFACE:
      default:
        return {width: this.UCASSISTANT_DEFAULT_WIDTH, height: this.UCASSISTANT_DEFAULT_HEIGHT}
    }
  }

  public backToDefaultWidth(win: BrowserWindow): void {
    let currentSize = win.getSize()
    let keepCurrentHeight = currentSize[1]
    
    switch (this.appInterface) {
      case this.UCASSISTANT_APPINTERFACE:
        this.updateSize(win, {width: this.UCASSISTANT_DEFAULT_WIDTH, height: keepCurrentHeight})
        break
      case this.CCAGENT_APPINTERFACE:
        this.updateSize(win, {width: this.CCAGENT_DEFAULT_WIDTH, height: keepCurrentHeight})
        break
      case this.SWITCHBOARD_APPINTERFACE:
        this.updateSize(win, {width: this.SWITCHBOARD_DEFAULT_WIDTH, height: keepCurrentHeight})
        break
    }
    this.saveWindowSize(win)
  }


  public removeMiniBarIfEnabled(win: BrowserWindow): void {
    if (this.appInterface == this.CCAGENT_MINIBAR_APPINTERFACE) {
      const { width, height } = screen.getPrimaryDisplay().workAreaSize
      win.setMaximumSize(width, height)
      this.updateAppInterface(win, this.CCAGENT_APPINTERFACE)
    }
  }
}