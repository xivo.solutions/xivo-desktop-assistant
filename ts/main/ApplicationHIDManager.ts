import { ipcMain, BrowserWindow } from "electron"
const path = require('path')

export default class HIDManager {

  private devicePicker: BrowserWindow | undefined
  private modalWidth: number = 340
  private modalHeight: number = 290
  
  constructor(private win: Electron.BrowserWindow) {}
  
  public createDevicePicker(): void {
    let bounds = this.win.getBounds()
    let x = bounds.x + ((bounds.width - this.modalWidth) / 2)
    let y = bounds.y + ((bounds.height - this.modalHeight) / 2)
    this.devicePicker = new BrowserWindow({
      modal: true,
      width: this.modalWidth,
      height: this.modalHeight,
      x: x,
      y: y,
      resizable: false,
      maximizable: false,
      fullscreenable: false,
      parent: this.win,
      backgroundColor: '#eeeeee',
      frame: false,
      title: '',
      webPreferences: {
        preload: (path.join(__dirname, "devicePicker/devicePickerPreload.js")),
        contextIsolation: true,
        nodeIntegration: true
      }
    })
    this.devicePicker.setMenuBarVisibility(false)
    this.devicePicker.loadFile(path.join(__dirname, "../../view/devicePickerView.html"))
  }

  public clearDevicePicker(): void {
    ipcMain.removeAllListeners("pickingReady")
    ipcMain.removeAllListeners("devicePicked")
    ipcMain.removeAllListeners("devicePickerClosed")
    this.devicePicker?.close()
    this.devicePicker = undefined
  }

  public sendDevicesToPicker(details: {deviceList: Array<any>}): void {
    this.devicePicker?.webContents.send("devices", details.deviceList)
  }
}