const electron = require('electron')

export default class PositionMgt {

  constructor(private win: Electron.BrowserWindow) {
    this.win.on('move', this.onMove.bind(this))
  }

  private readonly DEFAULT_TOLERANCE: number = 10
  private moving: any = null

  //Simulate moved event in win32
  private onMove(): void {
    if (this.moving) {
      clearTimeout(this.moving)
    }
    this.moving = setTimeout(this.onMoved.bind(this), 200)
  }

  private onMoved(): void {
    this.repositioning()
  }

  private getDisplay(id = undefined): Electron.Display | undefined {
    if (id === undefined) {
      return electron.screen.getDisplayMatching(this.win.getBounds())
    }
    else {
      let displays = electron.screen.getAllDisplays()
      for (let d in displays) {
        if (displays[d].id === id) {
          return displays[d]
        }
      }
    }
    return undefined
  }
  
  public getAlign(): Align | undefined {
    let winBox = new Box(this.win.getBounds())
    //Get the display in which the window is
    let display = this.getDisplay()
    if (display != undefined) {
      let displayBox = new Box(display.bounds)
      let align = undefined
      let verticalAlign = undefined
      if (displayBox.getLeft() === winBox.getLeft()) {
        align = 'left'
      }
      else if (displayBox.getRight() === winBox.getRight()) {
        align = 'right'
      }
      if (displayBox.getTop() === winBox.getTop()) {
        verticalAlign = 'top'
      }
      else if (displayBox.getBottom() === winBox.getBottom()) {
        verticalAlign = 'bottom'
      }
      return new Align(display.id, align, verticalAlign)
    } else return undefined;
  }

  public setAlign(align: any): void {
    let display = this.getDisplay(align.displayId)
    if (display != undefined) {
      let displayBox = new Box(display.bounds)
      let winBox = new Box(this.win.getBounds())
      if (align.align) {
        if (align.align === "left") {
          winBox.setLeft(displayBox.getLeft())
        }
        else if (align.align === "right") {
          winBox.setRight(displayBox.getRight())
        }
      }
      if (align.verticalAlign) {
        if (align.verticalAlign === "top") {
          winBox.setTop(displayBox.getTop())
        }
        else if (align.verticalAlign === "bottom") {
          winBox.setBottom(displayBox.getBottom())
        }
      }
      this.win.setBounds(winBox.rect)
    }
  }

  public repositioning(tolerance: number = this.DEFAULT_TOLERANCE): void {
    if (this.win && !this.win.isDestroyed()) {
      let winBox = new Box(this.win.getContentBounds())

      //Get the display in which the window is
      let display = this.getDisplay()
      if (!display) {
        return
      }
      let displayBox = new Box(display.bounds)

      //If box is not on display or near the border, position it just on the border
      if (winBox.getLeft() - displayBox.getLeft() < tolerance) {
        winBox.setLeft(displayBox.getLeft())
      }
      else if (displayBox.getRight() - winBox.getRight() < tolerance) {
        winBox.setRight(displayBox.getRight())
      }

      if (winBox.getTop() - displayBox.getTop() < tolerance) {
        winBox.setTop(displayBox.getTop())
      }
      else if (displayBox.getBottom() - winBox.getBottom() < tolerance) {
        winBox.setBottom(displayBox.getBottom())
      }
      this.win.setBounds(winBox.rect)
    }
  }
}


class Align {

  displayId: number
  align: any
  verticalAlign: any
  
  constructor(displayId: number, align: any, verticalAlign: any) {
    this.displayId = displayId
    this.align = align
    this.verticalAlign = verticalAlign
  }
}

class Box {

  rect: Electron.Rectangle
    
  constructor(rect: Electron.Rectangle) {
    this.rect = rect
  }

  getLeft(): number {
    return this.rect.x
  }

  setLeft(val: number): void {
    this.rect.x = val
  }

  getRight(): number {
    return this.rect.x + this.rect.width
  }

  setRight(val: number): void {
    this.rect.x = val - this.rect.width
  }

  getTop(): number {
    return this.rect.y
  }

  setTop(val: number): void {
    this.rect.y = val
  }

  getBottom(): number {
    return this.rect.y + this.rect.height
  }

  setBottom(val: number): void {
    this.rect.y = val - this.rect.height
  }
}