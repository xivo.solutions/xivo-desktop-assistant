const { ipcMain, autoUpdater, clipboard, app, Tray, nativeImage, globalShortcut } = require('electron')
const path = require('path')
const url = require('url')
const _ = require('lodash')
const os = require('os')
const log = require('electron-log')
import AutoLauncher from 'auto-launch'
import MenuTools from '../helpers/ApplicationMenuTools'
import PositionMgt from './ApplicationPositionDocking'
import SizeMgt from './ApplicationSizeManager'
import RegistryEditor from './RegistryEditor'
import ConfigManager from './ApplicationConfigManager'
import Zoom from 'helpers/ApplicationZoom'
import HIDManager from './ApplicationHIDManager'
import { FeedURLOptions } from 'electron'

export default class ApplicationEventsListener extends MenuTools {

  private readonly phoneEvent: string = 'PHONE_EVENT'
  private readonly jitsiEvent: string = 'JITSI_EVENT'
  private readonly acceptedProtocols: Array<string> = ["tel", "callto"]
  private readonly protocolSep: string = ":"
  private videoIsStarted: boolean = false

  constructor(
    protected win: Electron.BrowserWindow,
    private readonly launchOptions: XiVO.Options,
    private positionMgt: PositionMgt,
    protected tray: Electron.Tray,
    protected closeParams: XiVO.Options,
    private sizeMgt: SizeMgt,
    private registryEditor: RegistryEditor,
    private launch: AutoLauncher,
    private configManager: ConfigManager,
    protected zoom: Zoom,
    private hidManager: HIDManager
  ) { super(win, closeParams, tray) }

  private extractMessageFromURL(data: string | undefined): string {
    if (_.find(this.acceptedProtocols, (p: string): string => {
      return _.startsWith(data, p + this.protocolSep)
    })){
      return _.split(data, this.protocolSep, 2)[1]
    } else return ""
  }

  private setGlobalShortcut(browserWindow: Electron.BrowserWindow, keyCombination: Electron.Accelerator): void {
    globalShortcut.register(keyCombination, () => {
      let msg = clipboard.readText('selection')
      if (!msg) msg = 'none'
      log.info('received global key with content')
      this.forwardMessage(browserWindow, msg, this.phoneEvent)
    })
  }


  public getApplicationUrl(): string {
    return url.format({
      pathname: path.join(__dirname, '/../../index.html'),
      protocol: 'file:',
      slashes: true
    })
  }

  public setTrayIcon(icon: string): void {
    let trayImage = path.join(__dirname, '/../../img/xivo_' + icon)
    trayImage += (os.platform() === 'win32') ? '.ico' : '.png'
    if (this.tray && !this.tray.isDestroyed()) {
      this.tray.setImage(nativeImage.createFromPath(trayImage))
    } else {
      this.tray = new Tray(nativeImage.createFromPath(trayImage))
    }
  }

  public runExecutable(path: string, args: any): void {
    log.info('Running executable ', path, ' with args ', args)
    const process = require('child_process')
    process.spawn(path, args || [])
  }

  public initApplicationListeners(): void {

    this.tray.on('click', (): void => {
      this.win.isVisible() ? this.win.hide() : this.win.show()
    })

    this.win.once('ready-to-show', (): void => {
      this.win.show()
    })

    this.win.on('close', (event: Electron.Event): void => {
      event.preventDefault()
      log.info("Received event to close inner browser window")
      this.closeApp()
    })

    app.on('second-instance', (_, commandLine: Array<string>): any => {
      this.setFocus(this.win)
      if (commandLine.length > 1) {
        log.info('received URL to open')
        let command = this.extractMessageFromURL([...commandLine].pop()).replace(/\%20/g,'')
        if(commandLine.length >= 2){
          if (commandLine[commandLine.length - 1].includes("tel:")) command = 'tel:' + command
          if (commandLine[commandLine.length - 1].includes("callto:")) command = 'callto:' + command
        }
        this.forwardMessage(this.win, command, this.phoneEvent)
      }
    })

    ipcMain.on('setUpdateUrl', (_, url: FeedURLOptions): void => {
      log.info("setUpdateUrl", url)
      autoUpdater.setFeedURL(url)
      autoUpdater.checkForUpdates()
    })

    ipcMain.on('checkForUpdate', (_): void => {
      log.info("checking for update")
      autoUpdater.checkForUpdates()
    })

    ipcMain.on('reloadApp', (): void => {
      if (process.env.CI !== 'e2e') this.win.loadURL(this.getApplicationUrl())
    })

    ipcMain.on('urlError', (_, arg: any): void => {
      log.error("Fail to load URL", arg)
      this.setTrayIcon('logout')
    })

    ipcMain.on('setUpdateGlobalShortcut', (_, arg: any): void => {
      globalShortcut.unregisterAll()
      if (arg != null) {
        this.setGlobalShortcut(this.win, arg)
      }
    })

    ipcMain.on('setUpdateCloseTray', (_, arg: any): void => {
      this.closeParams.closeInTray = arg
    })

    ipcMain.on('setUpdateStartUp', (_, arg: Boolean): void => {
      this.launch.isEnabled()
        .then((isEnabled: boolean): void => {
          if (arg !== isEnabled) {
            (arg) ? this.launch.enable() : this.launch.disable()
          }
          if (arg == true && isEnabled == false && os.platform() === 'win32') this.registryEditor.overrideUpdaterPath()
        })
        .catch((err: Error): void => {
          log.error(err)
        })
    })

    ipcMain.on('updateBrowserWindowSize', (_, arg: any): void => {
      if (arg.mini) {
        this.sizeMgt.updateAppInterface(this.win, this.sizeMgt.CCAGENT_MINIBAR_APPINTERFACE)
        if (this.win.isMaximized()) this.sizeMgt.unmaximizeWindow(this.win, true)
        this.win.setMaximumSize(arg.w, arg.h)
      }
      else {
        this.sizeMgt.removeMiniBarIfEnabled(this.win)
        this.win.setMinimumSize(arg.w, arg.h)
      }
      let align = this.positionMgt.getAlign()
      this.positionMgt.setAlign(align)
      this.positionMgt.repositioning()
      this.win.minimizable = !arg.mini
      this.win.setAlwaysOnTop(arg.mini)
    })

    ipcMain.on('i18n', (event: Electron.IpcMainEvent): void => {
      event.returnValue = app.getLocale()
    })


    ipcMain.on('setConfirmQuit', (_, arg: any): void => {
      this.closeParams.confirm = !arg.reset ? arg : null
    })

    ipcMain.on('forceQuit', (): void => {
      this.forceQuit()
    })

    ipcMain.on('close', (event: Electron.Event): void => {
      this.closeApp(event as Event)
    })

    ipcMain.on('minimize', (): void => {
      this.win.minimize()
    })

    ipcMain.on('fullscreen-toggle', (): void => {
      if (this.win.isMaximized()) {
        if(this.videoIsStarted) {
         this.sizeMgt.unmaximizeWhileInVideo(this.win)
        } else {
          this.sizeMgt.unmaximizeWindow(this.win, true)
        }
      } else this.sizeMgt.maximizeWindow(this.win)
    })

    ipcMain.on('zoom-in', (): void => {
      this.zoom.zoomIn()
    })

    ipcMain.on('zoom-out', (): void => {
      this.zoom.zoomOut()
    })

    ipcMain.on('config', (): void => {
      this.win.webContents.send('sref', 'main.settings')
    })

    ipcMain.on('focusBrowserWindow', (): void => {
      this.setFocus(this.win)
    })

    ipcMain.on('trayIcon', (_, arg: any): void => {
      this.setTrayIcon(arg)
    })

    ipcMain.on('runExecutable', (_, path: string, args: any): void => {
      this.runExecutable(path, args)
    })

    ipcMain.on('quitAndInstall', (): void => {
      this.closeParams.quit = true
      autoUpdater.quitAndInstall()
    })

    ipcMain.on('startJitsi', (_, data: any): void => {
      this.win.webContents.send('openJitsi', data)
    })

    ipcMain.on('muteJitsi', (_): void => {
      this.win.webContents.send('muteJitsi')
    })

    ipcMain.on(this.jitsiEvent, (_, state): void => {
      switch (state) {
        case 'EventStartVideo':
          this.win.focus()
          this.win.webContents.executeJavaScript(`
            document.getElementById("webapp")?.classList.add("visio-style")
          `)
          this.sizeMgt.unmaximizeWhileInVideo(this.win) //force resize to not fall in bug: https://github.com/electron/electron/issues/28699
          this.sizeMgt.maximizeWindow(this.win)
          this.videoIsStarted = true
          break;
        case 'EventCloseVideo':
          this.videoIsStarted = false
          if (this.win.isMaximized()) this.sizeMgt.unmaximizeWindow(this.win)
          this.win.webContents.executeJavaScript(`
          document.getElementById("webapp")?.classList.remove("visio-style")
          `)
          this.sizeMgt.backToDefaultWidth(this.win)
        break;
      }
    })

    ipcMain.on('retrieveToken', () => {
      if (this.launchOptions.token != undefined && this.launchOptions.token != "" && this.launchOptions.token != null) {
        log.info(`sending token ${this.launchOptions.token} to renderer`)
        this.win.webContents.send("token", this.launchOptions.token)
      }
    })

    ipcMain.on('updateAppInterface', (_, arg: any) => {
      this.sizeMgt.updateAppInterface(this.win, arg || this.sizeMgt.UCASSISTANT_APPINTERFACE)
    })

    ipcMain.on('configListenerReady', () => {
      this.configManager.applyConfiguration(this.win)
    })

    ipcMain.on('setFullScreen', () => {
      if (!this.win.isMaximized()) {
        this.sizeMgt.maximizeWindow(this.win)
      }
    })

    ipcMain.on('clearLocalStorage', () => {
      return new Promise((resolve) => {
        this.win.webContents.send('clearLocalStorage')
        resolve("localstorage cleared")
      })
    })

    ipcMain.on('appInterface', (_, appInterface) => {
      this.sizeMgt.updateAppInterface(this.win, appInterface || this.sizeMgt.UCASSISTANT_APPINTERFACE)
    })

    ipcMain.on('allowDevice', (_) => {
      this.win.webContents.session.setDevicePermissionHandler((_) => {
          return true;
      });
      this.win.webContents.send('allowed')
    })

    ipcMain.on('requestWebHID', async (_, filters) => {
      log.info("Received WebHID request", filters)
      let formattedFilters = filters.map((id: number) => {
        return {vendorId: id}
      })
      this.hidManager.createDevicePicker()
      ipcMain.on('pickingReady', () => { this.win.webContents.send('askForWebHID', formattedFilters) })
      ipcMain.on('devicePickerClosed', () => { this.hidManager.clearDevicePicker() })
    })

    ipcMain.on('webHidError', (_, error) => {
      log.warn("Got web HID error", error)
    })

    this.win.webContents.session.on('select-hid-device', async (event, details, callback) => {
      if (details.deviceList && details.deviceList.length > 0) {
        event.preventDefault()
        this.hidManager.sendDevicesToPicker(details)
        ipcMain.on("devicePicked", (_, deviceId) => {
          callback(deviceId)
          this.hidManager.clearDevicePicker()
        })
      }
    })
  }

}
