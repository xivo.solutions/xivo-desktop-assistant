const { autoUpdater } = require('electron')
const log = require('electron-log')

export default class AutoUpdaterListener {
  constructor(public win: Electron.BrowserWindow) {}

  public initUpdaterListeners() {
    autoUpdater.addListener("update-available", () => {
      log.info("update-available")
    })

    autoUpdater.addListener("update-downloaded", () => {
      log.info("update-downloaded")
      this.win.webContents.send('sref', 'main.updateavailable')
    })

    autoUpdater.addListener('error', (_error: any) => {
      log.error("Error trying to update")
    })

    autoUpdater.addListener("checking-for-update", () => {
      log.info("update-checking")
    })

    autoUpdater.addListener("update-not-available", () => {
      log.info("update-not-available")
    })
  }
}
