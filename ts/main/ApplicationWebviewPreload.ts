const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('setElectronConfig', (data: any): void => {

  if (data.size) {
    ipcRenderer.send('updateBrowserWindowSize', data.size)
  }
  if (data.focus) {
    ipcRenderer.send('focusBrowserWindow')
  }
  if (data.confirmQuit) {
    ipcRenderer.send('setConfirmQuit', data.confirmQuit)
  }
  if (data.forceQuit) {
    ipcRenderer.send('forceQuit')
  }
  if (data.trayIcon) {
    ipcRenderer.send('trayIcon', data.trayIcon)
  }
  if (data.runExecutable) {
    ipcRenderer.send('runExecutable', data.runExecutable, data.executableArgs)
  }
  if (data.startJitsi) {
    ipcRenderer.send('startJitsi', data.startJitsi)
  }
  if (data.muteJitsi) {
    ipcRenderer.send('muteJitsi')
  }
  if(data.setFullScreen) {
    ipcRenderer.send('setFullScreen')
  }
  if(data.setUpdateUrl) {
    ipcRenderer.send('setUpdateUrl', data.updateURL, false)
  }
  if(data.requestWebHID) {
    ipcRenderer.send('requestWebHID', data.filters)
  }
})