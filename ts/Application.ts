/// <reference path="Application.d.ts" />
(function () { if (require('electron-squirrel-startup')) return })()

import ApplicationEventsListener from './main/ApplicationEventsListener'
import WebviewEventsListener from './main/WebviewEventsListener'
import AutoUpdaterListener from './main/AutoUpdaterListener'
import ApplicationMenu from './main/ApplicationMenu'
import ConfigManager from './main/ApplicationConfigManager'
import PositionMgt from './main/ApplicationPositionDocking'
import SizeMgt from './main/ApplicationSizeManager'
import RegistryEditor from './main/RegistryEditor'
import Zoom from './helpers/ApplicationZoom'
import HIDManager from './main/ApplicationHIDManager'

const { app, BrowserWindow, globalShortcut, Tray, nativeImage } = require('electron')
const path = require('path')
const url = require('url')
const os = require('os')
const windowStateKeeper = require('electron-window-state')
const fs = require('fs')
const log = require('electron-log')
const i18n = require('./helpers/translate')
const commandLineArgs = require('command-line-args')
const windowStateReset = app.getPath('appData') + '/xivo-desktop-assistant/window-state.lock'
const { setupScreenSharingMain } = require("@jitsi/electron-sdk")
const AutoLaunch = require('auto-launch')

const autoLauncher = new AutoLaunch({
  name: app.getName()
})

const optionDefinitions: Array<XiVO.OptionsDefinition> = [
  {
    name: 'debug',
    alias: 'd',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'ignore-certificate-errors',
    alias: 'i',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'token',
    alias: 't',
    type: String
  },
  {
    name: 'squirrel-install',
    alias: 'n',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'squirrel-firstrun',
    alias: 'f',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'squirrel-uninstall',
    alias: 'u',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'inspect',
    alias: 'I',
    type: Boolean,
    defaultValue: false
  }
]

const acceptedProtocols: Array<string> = ["tel", "callto"]
const appName: string = app.getName()
const logsPath = app.getPath('appData') + '/xivo-desktop-assistant/application/logs'

let launchOptions: XiVO.Options = {
  debug: false,
  ignoreCertificateErrors: false
}

let closingParameters: XiVO.Options = {
  closeInTray: false,
  quit: false,
  confirm: undefined
}

if (!fs.existsSync(logsPath)) {
  fs.mkdirSync(logsPath, { recursive: true })
}

log.transports.file.resolvePath = () => logsPath + '/application.log'
log.variables.appversion = app.getVersion();
log.transports.file.format = '[{y}-{m}-{d} {h}:{i}:{s}] [{appversion}] [{level}] {text}'
log.transports.file.maxSize = 10485760 // 10 MiB

const setArgs = (): void => {
  try {
    launchOptions = commandLineArgs(optionDefinitions, {'argv': process.argv})
    log.info('Starting with options: ', launchOptions, ' on :', process.argv)
    if (launchOptions["ignore-certificate-errors"]) {
      app.commandLine.appendSwitch('ignore-certificate-errors', 'true')
    }
  }
  catch (err) {
    log.error('Unable to parse arguments, ignoring all.')
    log.error('Details: ', err)
  }

  if (typeof(process.env.CUSTOM_USER_DATA) !== "undefined") {
    app.setPath('userData', process.env.CUSTOM_USER_DATA)
  }
}

const getApplicationUrl = (view: string): string => {
  return url.format({
    pathname: path.join(__dirname, '/../index.html'),
    protocol: 'file:',
    slashes: true,
    hash: view
  })
}


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: Electron.BrowserWindow
let tray: Electron.Tray
let positionMgt: PositionMgt
let sizeMgt: SizeMgt
let applicationEventsListener: ApplicationEventsListener
let applicationMenu: ApplicationMenu
let webviewEventsListener: WebviewEventsListener
let autoUpdaterListener: AutoUpdaterListener
let registryEditor: RegistryEditor
let configManager: ConfigManager
let zoom: Zoom
let hidManager: HIDManager


const setTrayIcon = (icon: string): void => {
  let trayImage = path.join(__dirname, '/../img/xivo_'+icon)
  trayImage += (os.platform() === 'win32') ? '.ico' : '.png'
  if (tray) {
    tray.setImage(nativeImage.createFromPath(trayImage))
  } else {
    tray = new Tray(nativeImage.createFromPath(trayImage))
  }
}

const createWindow = (): void => {
  i18n.setLocale(app.getLocale())
  configManager = new ConfigManager()
  configManager.initApplicationConfigFile()
  sizeMgt = new SizeMgt()

  let mainWindowState = windowStateKeeper({
    defaultWidth: sizeMgt.getAppInterfaceSize().width,
    defaultHeight: sizeMgt.getAppInterfaceSize().height,
    path: app.getPath('userData'),
    maximize: false,
    fullScreen: false
  })

  if (!fs.existsSync(windowStateReset)) {
    mainWindowState.resetStateToDefault()
    fs.closeSync(fs.openSync(windowStateReset, 'w'));
  }

  log.info("Creating new BrowserWindow")
  win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    minWidth: sizeMgt.getAppInterfaceSize().width,
    minHeight: sizeMgt.getAppInterfaceSize().height,
    resizable: true,
    maximizable: true,
    fullscreenable: true,
    frame: false,
    backgroundColor: '#eeeeee',
    show: false,
    webPreferences: {
      preload: (path.join(__dirname, "renderer/ApplicationAngular.js")),
      webviewTag: true,
      contextIsolation: false,
      spellcheck: false,
      nodeIntegration: true,
    }
  })


    mainWindowState.manage(win)


    setTrayIcon('default')
    tray.setToolTip(i18n.__('APP_NAME'))


  win.loadURL(getApplicationUrl('/'))
  let currentSession = win.webContents.session
  currentSession.clearCache()
  .then(() => { log.info("Application cache cleared.")})
  .catch(() => { log.error("Could not clean up application cache, aborting.")})

    positionMgt = new PositionMgt(win)
    zoom = new Zoom(win, sizeMgt)
    hidManager = new HIDManager(win)
    applicationMenu = new ApplicationMenu(win, closingParameters, tray, zoom)
    applicationMenu.rightClickEventListener()
    registryEditor = new RegistryEditor(appName, app)
    applicationEventsListener = new ApplicationEventsListener(win, launchOptions, positionMgt, tray, closingParameters, sizeMgt, registryEditor, autoLauncher, configManager, zoom, hidManager)
    applicationEventsListener.initApplicationListeners()
    webviewEventsListener = new WebviewEventsListener()
    webviewEventsListener.initWebviewListeners()
    autoUpdaterListener = new AutoUpdaterListener(win)
    autoUpdaterListener.initUpdaterListeners()
    tray.setContextMenu(applicationMenu.contextMenu)

  setupScreenSharingMain(win, appName)
  registryEditor.setProtocols(acceptedProtocols)

  autoLauncher.isEnabled().then((isEnabled: boolean): void => {
    if (os.platform() === 'win32' && isEnabled) registryEditor.overrideUpdaterPath()
  })

  log.info("Browserwindow created, TS modules loaded")
}

app.setAppUserModelId('com.squirrel.' + appName + '.' + appName)

setArgs()

app.commandLine.appendSwitch('force-device-scale-factor', '1')
app.commandLine.appendSwitch('disable-site-isolation-trials')

app.on('ready', () => {
  const firstInstance = app.requestSingleInstanceLock()
  if (!firstInstance) {
    log.info("Closing app: app is already running.")
    globalShortcut.unregisterAll()
    app.quit()
  } else {
    createWindow()
  }
})
