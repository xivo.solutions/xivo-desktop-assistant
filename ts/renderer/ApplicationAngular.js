"use strict";

const { ipcMain } = require('electron');
const log = require('electron-log')

window.onload = () => {
  window.$ = window.jQuery = require("jquery");
  require("bootstrap");
  require("angular");
  require("angular-translate");
  require("angular-ui-router");
  require("ui-select");
  require("module").globalPaths.push(__dirname);
  const ipcRenderer =
    require("electron").ipcRenderer || window.require("electron").ipcRenderer; // to be able to mock it (see angular-helper)

  const isAccelerator = require("electron-is-accelerator");
  const _ = require("lodash");
  const Shortcuts = require("../helpers/ApplicationShortcuts");
  const { setupScreenSharingRender } = require("@jitsi/electron-sdk");

  // proxy inbetween real ipcrenderer with filesystem access and the one called from the loaded webview
  window.ipcRenderer = {
    send: (data) => {
      switch (data) {
      case "config":
      case "minimize":
      case "close":
      case "zoom-in":
      case "zoom-out":
      case "fullscreen-toggle":
        ipcRenderer.send(data);
        break;
      default:
        console.error(
          "ipcRenderer wrapper does not support the following action : ",
          data
        );
        break;
      }
    },
  };

    const i18n = require("../helpers/translate");
    const shortcuts = new Shortcuts();

    const desktopApp = angular
        .module("desktopApp", ["pascalprecht.translate", "ui.router", "ui.select"])
        .config(function ($translateProvider) {
            $translateProvider.useMissingTranslationHandler("i18nProxy");
            // force locale to be set before $translate is instantiated
            i18n.setLocale(ipcRenderer.sendSync("i18n", "locale"));
        })
        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state("main", {
                    url: "/",
                    templateUrl: "view/main.tpl.html",
                    controller: "MainController",
                })
                .state("main.settings", {
                    templateUrl: "view/main.settings.tpl.html",
                    controller: "SettingsController",
                })
                .state("main.updateavailable", {
                    templateUrl: "view/main.updateavailable.tpl.html",
                    controller: "UpdateAvailableController",
                })
                .state("loadingerror", {
                    url: "/loadingerror",
                    templateUrl: "view/loadingerror.tpl.html",
                    controller: "LoadingErrorController",
                });

            $urlRouterProvider.otherwise("/");
        })
        .run(function ($rootScope) {
            $rootScope.title = i18n.__("APP_NAME");
        });

    desktopApp.factory("i18nProxy", function () {
    return function (translationId, $uses, interpolateParams) {
      return i18n.__(translationId, interpolateParams);
    };
  });

  desktopApp.factory("desktopSettings", function (updateService) {
    const _KEY_APP_PROTOCOL = "APP_PROTOCOL";
    const _KEY_APP_SERVER = "APP_DOMAIN";
    const _KEY_ALT_SERVERS = "ALT_SERVERS";
    const _KEY_ALT_SERVER_SELECTED = "ALT_SERVER_SELECTED";
    const _KEY_APP_INTERFACE = "APP_INTERFACE";
    const _KEY_APP_SHORTCUT = "APP_SHORTCUT";
    const _KEY_APP_CLOSE = "APP_CLOSE";
    const _KEY_APP_STARTUP = "APP_STARTUP";
    const _KEY_APP_TOKEN = "APP_TOKEN";
    const _KEY_HID_DEVICE = "HID_DEVICE";
    const _INTERFACE_CCAGENT = "ccagent";
    const _INTERFACE_UCASSISTANT = "ucassistant";
    const _INTERFACE_SWITCHBOARD = "switchboard";
    const _DEFAULT_SHORTCUT = "CmdOrCtrl+Space";
    const interfaces = [_INTERFACE_UCASSISTANT, _INTERFACE_CCAGENT, _INTERFACE_SWITCHBOARD]

    const _setLocalstorageConfig = (data) => {
      if (data.length > 0) {
        data.forEach((item) => {
          let [key, value] = item;
          localStorage.setItem(key, value);
          console.log(`Set localstorage config ${key} -> ${value}`);
        });
        _applyLocalStorageConfig()
      }
    }

    const _hasDifferencesInLocalStorage = (data) =>
    (_altServersEnabled() ? data.filter(([key, value]) => key !== "APP_DOMAIN") : data)
      .some(([key, value]) =>
        (typeof value == "object" && _hasDifferencesInLocalStorage(Object.entries())) ||
        (localStorage.getItem(key)|| '').toString() !== (value.toString() || '')
    );


    ipcRenderer.on("handleLocalStorageConfig", (event, data) => {
      if(_hasDifferencesInLocalStorage(data)) {
        log.info("Updating local storage with new config")
        _setLocalstorageConfig(data)
      }
    })


    const _applyLocalStorageConfig = () => {
      updateService.setUpdateStartUp(_get(_KEY_APP_STARTUP));
      updateService.setUpdateCloseTray(_get(_KEY_APP_CLOSE));
      updateService.updateAppInterface(_get(_KEY_APP_INTERFACE));
      updateService.reloadApp();
    }

    ipcRenderer.send("configListenerReady");

    const _get = function (key) {
      return localStorage.getItem(key);
    };

    const _set = function (key, value) {
      if (value != undefined) return localStorage.setItem(key, value);
      else return localStorage.removeItem(key);
    };

    const _generateAltServers = (altServerRaw) => {
      let parsedServers = JSON.parse(altServerRaw);
      if (parsedServers && Object.keys(parsedServers).length > 0) {
        return Object.entries(parsedServers).map(([key, value], index) => {
          return {id: index, displayName: key, server: value};
        });
      } else return {};
    };

    const altServers = _generateAltServers(_get(_KEY_ALT_SERVERS) || "{}");

    const _getAltServerList = () => {
      return altServers;
    };

    const _altServersEnabled = () => {
      return Object.keys(altServers).length > 0;
    };

    const _getSelectedAltServer = () => {
      return altServers[_get(_KEY_ALT_SERVER_SELECTED)] ? altServers[_get(_KEY_ALT_SERVER_SELECTED)] : altServers[0];
    };

    const _getServerAddressOnly = (serverAddress) => {
      return _getServerAddressWithoutInterface(
        _getServerAddressWithoutProtocol(serverAddress)
      );
    };

    const _getServerAddressWithoutInterface = (serverAddress) => {
      if (!serverAddress) {
        return undefined;
      }
      let interfaceInServerPath = serverAddress.split("/").pop();
      if (
        interfaceInServerPath === _INTERFACE_CCAGENT ||
        interfaceInServerPath === _INTERFACE_UCASSISTANT ||
        interfaceInServerPath === _INTERFACE_SWITCHBOARD
      ) {
        serverAddress = serverAddress.split("/").slice(0, -1).join("/");
      }
      return serverAddress.endsWith("/")
        ? serverAddress.slice(0, -1)
        : serverAddress;
    };

    const _getServerAddressWithoutProtocol = (serverAddress) => {
      if (!serverAddress) {
        return undefined;
      }
      if (
        serverAddress.startsWith("http://") ||
        serverAddress.startsWith("https://")
      ) {
        serverAddress = serverAddress.split("//").slice(1).join("/");
      }
      return serverAddress;
    };

    const _getServerAddressProtocol = (serverAddress) => {
      if (serverAddress) {
        if (serverAddress.startsWith("http://")) {
          return "http";
        }
      }
      return "https";
    };

    const _getInterfaceFromServerAddress = (serverAddress) => {
      let serverUrl = new URL(serverAddress);
      let appInterface = serverUrl.pathname.split("/").pop();
      if (
        appInterface === _INTERFACE_CCAGENT ||
        appInterface === _INTERFACE_UCASSISTANT ||
        appInterface === _INTERFACE_SWITCHBOARD
      ) {
        return appInterface;
      }
    };

    const _getInterface = () => {
      let appInterface = _get(_KEY_APP_INTERFACE);
      let serverAddress = _get(_KEY_APP_SERVER);
      let serverProtocol = _get(_KEY_APP_PROTOCOL);
      if (appInterface) {
        return appInterface;
      } else if (serverAddress) {
        appInterface = _getInterfaceFromServerAddress(
          serverProtocol + "://" + serverAddress
        );
        return !appInterface ? _INTERFACE_UCASSISTANT : appInterface;
      }
      return _INTERFACE_UCASSISTANT;
    };

    console.log("renderer desktopSettings loaded")

    return {
      KEY_APP_PROTOCOL: _KEY_APP_PROTOCOL,
      KEY_APP_SERVER: _KEY_APP_SERVER,
      KEY_ALT_SERVERS: _KEY_ALT_SERVERS,
      KEY_ALT_SERVER_SELECTED: _KEY_ALT_SERVER_SELECTED,
      KEY_APP_INTERFACE: _KEY_APP_INTERFACE,
      KEY_APP_SHORTCUT: _KEY_APP_SHORTCUT,
      KEY_APP_CLOSE: _KEY_APP_CLOSE,
      KEY_APP_TOKEN: _KEY_APP_TOKEN,
      KEY_APP_STARTUP: _KEY_APP_STARTUP,
      KEY_HID_DEVICE: _KEY_HID_DEVICE,
      DEFAULT_SHORTCUT: _DEFAULT_SHORTCUT,
      INTERFACE_CCAGENT: _INTERFACE_CCAGENT,
      INTERFACE_UCASSISTANT: _INTERFACE_UCASSISTANT,
      INTERFACE_SWITCHBOARD: _INTERFACE_SWITCHBOARD,
      interfaces: interfaces,
      getServerAddressOnly: _getServerAddressOnly,
      getInterfaceFromServerAddress: _getInterfaceFromServerAddress,
      getServerAddressWithoutInterface: _getServerAddressWithoutInterface,
      getServerAddressWithoutProtocol: _getServerAddressWithoutProtocol,
      getServerAddressProtocol: _getServerAddressProtocol,
      getInterface: _getInterface,
      getAltServerList: _getAltServerList,
      getSelectedAltServer: _getSelectedAltServer,
      altServersEnabled: _altServersEnabled,
      generateAltServers: _generateAltServers,
      get: _get,
      set: _set,
      setLocalstorageConfig: _setLocalstorageConfig,
      hasDifferencesInLocalStorage: _hasDifferencesInLocalStorage,
      applyLocalStorageConfig: _applyLocalStorageConfig
    };
  });

  desktopApp.factory("updateService", () => {
      let error = false;

    const _setUpdateUrl = (url) => {
      ipcRenderer.send("setUpdateUrl", url);
    };

    const _checkForUpdate = () => {
      ipcRenderer.send("checkForUpdate");
    };

    const _reloadApp = () => {
      ipcRenderer.send("reloadApp");
    };

    const _setUpdateShortcut = (keyCombination) => {
      ipcRenderer.send(
        "setUpdateGlobalShortcut",
        keyCombination !== "null" ? keyCombination : null
      );
    };

    const _updateAppInterface = (appInterface) => {
      ipcRenderer.send(
        "updateAppInterface",
          appInterface
      );
    };

    const _setUpdateCloseTray = (isTray) => {
      ipcRenderer.send("setUpdateCloseTray", isTray);
    };

    const _setUpdateStartUp = (isBoot) => {
      ipcRenderer.send("setUpdateStartUp", isBoot);
    };

    const _throwUrlError = (url) => {
      ipcRenderer.send("urlError", url);
    };

    const _setError = () => {
      error = true;
    };

    const _hasError = () => {
      return error;
    };

    const _clearError = () => {
      error = false;
    };

    const _getUpdateShortcut = (shortcut) => {
      return _.join(
        _.map(shortcut, (key) => {
          return key.id;
        }),
        "+"
      );
    };

    const _displayShortcut = (shortcut) => {
      return _.join(
        _.map(shortcut, (key) => {
          return key.label;
        }),
        " "
      );
    };

    const _buildKeyList = () => {
        const localizedModifiers = _.map(
            shortcuts.LOCALIZED_MODIFIER_KEYS,
            (value) => {
                return {
                    id: value,
                    label: i18n.__("SETTINGS_SHORTCUT_KEY_" + value) + " +",
                };
            }
        );
        const localizedKeys = _.map(shortcuts.LOCALIZED_KEYS, (value) => {
            return {id: value, label: i18n.__("SETTINGS_SHORTCUT_KEY_" + value)};
        });
        const commonKeys = _.map(shortcuts.COMMON_KEYS, (value) => {
            return {id: value, label: value};
        });

        return _.concat(localizedModifiers, localizedKeys, commonKeys);
    };

    const _quitAndInstall = () => {
      ipcRenderer.send("quitAndInstall");
    };

    console.log("renderer updateService loaded")

    return {
      setUpdateUrl: _setUpdateUrl,
      setUpdateShortcut: _setUpdateShortcut,
      checkForUpdate: _checkForUpdate,
      setUpdateCloseTray: _setUpdateCloseTray,
      setUpdateStartUp: _setUpdateStartUp,
      getUpdateShortcut: _getUpdateShortcut,
      displayShortcut: _displayShortcut,
      buildKeyList: _buildKeyList,
      quitAndInstall: _quitAndInstall,
      throwUrlError: _throwUrlError,
      hasError: _hasError,
      setError: _setError,
      clearError: _clearError,
      reloadApp: _reloadApp,
      updateAppInterface: _updateAppInterface,
    };
  });

  desktopApp.directive("validShortcut", function (updateService) {
    return {
      require: "ngModel",
      link: function ($scope, elm, attrs, ctrl) {
        $scope.$watch(attrs.ngModel, function (viewValue) {
          ctrl.$setValidity(
            "format",
            _.isEmpty(_.trim(viewValue)) ||
              isAccelerator(updateService.getUpdateShortcut(viewValue))
          );
        });
      },
    };
  });

  desktopApp.controller(
    "MainController",
    function MainController(
      $scope,
      desktopSettings,
      $state,
      updateService,
      $log,
      $window,
      $rootScope
    ) {
      const webview = document.querySelector("webview");
      let protocol = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL);
      let serverAddress = desktopSettings.get(desktopSettings.KEY_APP_SERVER);
      $scope.appInterface = desktopSettings.get(desktopSettings.KEY_APP_INTERFACE);
      let pairedDevice = desktopSettings.get(desktopSettings.KEY_HID_DEVICE);
      $rootScope.displayedServerName = ' ';
      let jitsiApiObj;

      if (pairedDevice) ipcRenderer.send("allowDevice")

      ipcRenderer.on("allowed", () => {
        navigator.hid.getDevices().then(devices => {
          let parsedDevice = JSON.parse(pairedDevice);
          let matchingDevice = devices.find(device => device.vendorId == parsedDevice.vendorId && device.productId == parsedDevice.productId)
          if (matchingDevice) {
            this.device = matchingDevice;
            openAndListenForDevice(this.device)
          }
        })
      })
      
      ipcRenderer.send('appInterface', localStorage.getItem(desktopSettings.KEY_APP_INTERFACE))

      const protocolOrServerAddressIsUnset = (protocol, serverAddress) => {
        return protocol == null || serverAddress == null;
      };

      const updateAppInterface = () => {
        let newAppInterface = desktopSettings.getInterface();
        desktopSettings.set(desktopSettings.KEY_APP_INTERFACE, newAppInterface);
        return newAppInterface;
      };

      const updateServerAddress = (serverAddress) => {
        let serverAddressOnly =
          desktopSettings.getServerAddressOnly(serverAddress);
        desktopSettings.set(desktopSettings.KEY_APP_SERVER, serverAddressOnly);
        return serverAddressOnly;
      };

      const getApplicationUrl = (protocol, serverAddress, appInterface, token) => {
        let url = protocol + "://" + serverAddress

        if (appInterface === undefined) url += "/ucassistant"
        else url += `/${appInterface}`

        if (token != undefined && token != "" && token != null) url += `?token=${token}`

        return url
      };

      const dismissVideo = () => {
        if (jitsiApiObj) jitsiApiObj.dispose();
        jitsiApiObj = undefined;
        window.focus();
        publishEvent("JITSI_EVENT", "EventCloseVideo");
        $scope.showPhoneBar = false;
        $scope.isToggled = false;
        if (!$rootScope.$$phase) $scope.$apply();
      };

      const onKickedOut = (event) => {
        event.kicker.displayName = jitsiApiObj.getDisplayName(event.kicker.id);
        publishEvent("JITSI_EVENT", JSON.stringify({event: "EventKicked", eventFromJitsi: event}));
        dismissVideo();
      };

      const sendToWebview = (message) => {
        if (process.env.CI == 'e2e') return message
        webview.executeJavaScript("window.postMessage(" + message + ', "/")')
      };

      const publishEvent = (type, value) =>  {
        ipcRenderer.send(type, value);
        sendToWebview(`{'type': '${type}', 'value': '${value}'}`);
      };

      const onIframeLoaded = () => {
        publishEvent("JITSI_EVENT", "JITSI_IFRAME_LOADED");
        $scope.showPhoneBar = true;
        if (!$rootScope.$$phase) $scope.$apply();
      };

      $scope.updateToken = (token) => {
        desktopSettings.set(desktopSettings.KEY_APP_TOKEN, token);
      }

      $scope.setKeys = (protocol, serverAddress) => {
        if ($scope.appInterface === undefined) {
          $scope.appInterface = updateAppInterface();
        }
        $scope.protocol = protocol;
        $scope.domain = updateServerAddress(serverAddress);
        $scope.url = getApplicationUrl(protocol, $scope.domain, $scope.appInterface, desktopSettings.get(desktopSettings.KEY_APP_TOKEN));
      };

      $scope.getJitsiApiUrl = () => {
        return (
          $scope.protocol + "://" + $scope.domain + "/video/external_api.js"
        );
      };

      $scope.togglePhoneApp = () => {
        $scope.isToggled = !$scope.isToggled;
      };

      $scope.throwWebviewError = () => {
        $log.error("Error failed to load webview - " + webview.src);
        updateService.setError();
        ipcRenderer.send("confirmQuit", { reset: true });
        ipcRenderer.send("webviewLoadFail");
        if ($state.current.name !== "main.settings") {
          $state.go("loadingerror");
          updateService.throwUrlError(webview.src);
        }
      };

      $scope.successWebviewLoad = () => {
        $log.info("Webview is loaded");
        updateService.clearError();
      };

      $scope.handleToken = (token) => {
        if (token !== desktopSettings.get(desktopSettings.KEY_APP_TOKEN)) {
          $scope.updateToken(token);
          updateService.reloadApp();
        }
      };

      const openAndListenForDevice = (device) => {
        device.open().then(() => {
          new window.Notification(
            "Desktop headset control", 
            { body: `Device ${device.productName} is now paired` }
          )
          publishEvent('WEBHID_DEVICE_SELECTED', device.productName);
        }).catch(e => console.error(e));

        device.addEventListener('inputreport', (e) => {
          const { data, reportId } = e;
          let eventCode;
          if (data.buffer) {
            let uia = new Uint8Array(data.buffer)
            eventCode = uia[0]
          } else eventCode = data.getUint8(0)
          if (eventCode == 0) return
          let report = getWebReport(reportId)

          publishEvent('WEBHID_INPUT', JSON.stringify({vendorId: device.vendorId, code: eventCode, reportId: reportId, report: report}));
        })
      }

      const getWebReport = (reportId) => {
        let report = this.device?.collections
          .flatMap(col => col.inputReports)
          .find(report => report.reportId === reportId)

        if (report && report.items?.length > 1) {
          report.items[0].usages = report.items.reduce((acc, item) => acc.concat(item.usages), []);
          report.items = [report.items[0]]
        }

        return report
      }

      webview.addEventListener("did-fail-load", () => {
        $scope.throwWebviewError();
      });

      webview.addEventListener("load-commit", () => {
        $scope.successWebviewLoad();
      });

      ipcRenderer.on("sref", (event, location) => {
        $state.go(location);
      });

      ipcRenderer.on("msg", (event, data) => {
        return sendToWebview(data)
      });

      ipcRenderer.on("openJitsi", (event, data) => {
        const options = {
          roomName: data.uuid,
          parentNode: document.querySelector("#meet"),
          userInfo: { displayName: data.user },
          jwt: data.token,
          configOverwrite: { subject: data.displayname, prejoinConfig: { enabled: false } },
          onload: onIframeLoaded
        };

        jitsiApiObj = new $window.JitsiMeetExternalAPI(
          $scope.domain + "/video",
          options
        );
        jitsiApiObj.addListener('readyToClose', dismissVideo);
        jitsiApiObj.addListener('participantKickedOut', onKickedOut);
        setupScreenSharingRender(jitsiApiObj);
        publishEvent('JITSI_EVENT', 'EventStartVideo');
      });

      ipcRenderer.on("muteJitsi", () => {
        if (jitsiApiObj) {
          jitsiApiObj.isAudioMuted().then(muted => {
            if (!muted) jitsiApiObj.executeCommand('toggleAudio');
          });
        }
      });

      ipcRenderer.on('token', (event, token) => $scope.handleToken(token));

      ipcRenderer.on('clearLocalStorage', (event) => {
        localStorage.clear()
      })

      ipcRenderer.on('askForWebHID', (_, filters) => {
        navigator.hid.requestDevice({filters})
        .then(devices => {
          let device = devices && devices.length > 0 ? devices[0] : undefined
          if (device) {
            desktopSettings.set(desktopSettings.KEY_HID_DEVICE, JSON.stringify({vendorId: device.vendorId, productId: device.productId}))
            openAndListenForDevice(device)
            this.device = device
          }
        })
        .catch(e => {
          ipcRenderer.send("webHidError", e)
        })
      })

      let close = desktopSettings.get(desktopSettings.KEY_APP_CLOSE) === "true";
      updateService.setUpdateCloseTray(close);

      if (desktopSettings.altServersEnabled()) {
        let selectedServer = desktopSettings.getSelectedAltServer();
        if (selectedServer) {
          serverAddress = desktopSettings.getSelectedAltServer().server;
          $rootScope.displayedServerName = ' - ' + desktopSettings.getSelectedAltServer().displayName;
        } else serverAddress = undefined;
      }

      if (protocolOrServerAddressIsUnset(protocol, serverAddress)) {
        $state.go("main.settings");
      } else {
        $scope.setKeys(protocol, serverAddress);
        ipcRenderer.send("retrieveToken");
      }

      let shortcut =
        desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) ||
        desktopSettings.DEFAULT_SHORTCUT;
      updateService.setUpdateShortcut(shortcut);

      $scope.showWebApp = () => {
        return $state.current.name === "main";
      };

      console.log("renderer MainController loaded")
    }
  );

  desktopApp.controller(
    "SettingsController",
    function SettingsController(
      $scope,
      desktopSettings,
      $state,
      updateService
    ) {

      $scope.protocol =
        desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL) ||
        desktopSettings.getServerAddressProtocol(
          desktopSettings.get(desktopSettings.KEY_APP_SERVER)
        );
      $scope.serverAddress = desktopSettings.getServerAddressOnly(
        desktopSettings.get(desktopSettings.KEY_APP_SERVER)
      );
      $scope.appInterface =
        desktopSettings.get(desktopSettings.KEY_APP_INTERFACE) ||
        desktopSettings.getInterface(desktopSettings.KEY_APP_SERVER);
      $scope.close =
        desktopSettings.get(desktopSettings.KEY_APP_CLOSE) === "true";
      $scope.startup =
        desktopSettings.get(desktopSettings.KEY_APP_STARTUP) === "true";
      $scope.keyList = updateService.buildKeyList();
      $scope.interfaces = desktopSettings.interfaces;
      $scope.selectedInterfaceWrapper = {selected: $scope.appInterface};
      $scope.shortcut = {};
      $scope.altServers = [];

      if (desktopSettings.altServersEnabled()) {
        $scope.altServers = desktopSettings.getAltServerList();
        $scope.selectedAltServer = desktopSettings.getSelectedAltServer();
      }

      let shortcut =
        desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) ||
        desktopSettings.DEFAULT_SHORTCUT;
      if (shortcut === "null" || !isAccelerator(shortcut)) {
        $scope.shortcut.current = null;
      } else {
        $scope.shortcut.current = _.map(_.split(shortcut, "+"), function (key) {
          return _.find($scope.keyList, function (item) {
            return key === item.id;
          });
        });
      }

      const _getCurrentInterface = function () {
        if ($scope.selectedInterfaceWrapper.selected) {
          return $scope.selectedInterfaceWrapper.selected;
        }
        return desktopSettings.getInterface() || "ucassistant";
      };

      $scope.altServersEnabled = () => {
        return desktopSettings.altServersEnabled();
      };

      $scope.save = function () {
        let shortcut = updateService.getUpdateShortcut($scope.shortcut.current);
        if (_.isEmpty(_.trim(shortcut))) {
          desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, "null");
        }
        if (isAccelerator(shortcut)) {
          desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, shortcut);
          updateService.setUpdateShortcut(shortcut);
        }
        desktopSettings.set(desktopSettings.KEY_APP_STARTUP, $scope.startup);
        updateService.setUpdateStartUp($scope.startup);
        desktopSettings.set(desktopSettings.KEY_APP_CLOSE, $scope.close);
        updateService.setUpdateCloseTray($scope.close);
        desktopSettings.set(desktopSettings.KEY_APP_PROTOCOL, $scope.protocol);
        $scope.appInterface = _getCurrentInterface();
        $scope.serverAddress = desktopSettings.getServerAddressWithoutInterface(
          $scope.serverAddress
        );
        desktopSettings.set(
          desktopSettings.KEY_APP_SERVER,
          $scope.serverAddress ? $scope.serverAddress : undefined
        );
        desktopSettings.set(desktopSettings.KEY_APP_INTERFACE, $scope.appInterface);

        let url;

        if (desktopSettings.altServersEnabled()) desktopSettings.set(desktopSettings.KEY_ALT_SERVER_SELECTED, $scope.selectedAltServer.id);

        updateService.updateAppInterface($scope.appInterface);
        updateService.reloadApp();
      };

      $scope.displayShortcutError = function () {
        return (
          updateService.displayShortcut($scope.shortcut.current) +
          " " +
          i18n.__("SETTINGS_SHORTCUT_WRONG_FORMAT")
        );
      };

      $scope.groupKey = function (key) {
        return shortcuts.LOCALIZED_MODIFIER_KEYS.indexOf(key.id) > -1
          ? i18n.__("SETTINGS_SHORTCUT_MODIFIERS")
          : i18n.__("SETTINGS_SHORTCUT_KEYS");
      };

      console.log("renderer SettingsController loaded")
    }
  );

  desktopApp.controller(
    "LoadingErrorController",
    function SettingsController(
      $scope,
      desktopSettings,
      $state,
      $timeout,
      updateService
    ) {
        const _retryTimeout = 60;
        $scope.retry = { delay: _retryTimeout };

        const _retryCountdown = function () {
            $scope.retry.delay--;
            if (
                $scope.retry.delay <= 0 &&
                $state.current.name !== "main.settings"
            ) {
                updateService.reloadApp();
            } else {
                $timeout(_retryCountdown, 1000);
            }
        };

        ipcRenderer.on("sref", (event, location) => {
        $state.go(location);
      });

      $scope.remoteUrl =
        desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL) +
        "://" +
        desktopSettings.get(desktopSettings.KEY_APP_SERVER);
      $timeout(_retryCountdown, 1000);
    }
  );

  desktopApp.controller(
    "UpdateAvailableController",
    function UpdateAvailableController($scope, updateService) {
      $scope.quitAndInstall = function () {
        updateService.quitAndInstall();
      };
    }
  );

  desktopApp.filter("trusted", [
    "$sce",
    function ($sce) {
      return function (url) {
        return $sce.trustAsResourceUrl(url);
      };
    },
  ]);

  desktopApp.directive("dynamicScript", function dynamicScript() {
    return {
      restrict: "E",
      scope: {
        dynamicScriptSrc: "=",
      },
      link: function (scope, element, attrs) {
        function setScriptSrc(element, src) {
          if (src) {
              const newScript = document.createElement("script");
              newScript.type = "text/javascript";
            newScript.src = src;
            element.empty();
            element.append(newScript);
          }
        }

        setScriptSrc(element, scope.$eval(attrs.dynamicScriptSrc));

        scope.$watch("dynamicScriptSrc", (src) => {
          setScriptSrc(element, src);
        });
      },
    };
  });

  console.log("renderer script loaded")
};
