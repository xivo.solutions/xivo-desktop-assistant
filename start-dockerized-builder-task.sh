#!/bin/bash
set -ex

cleanup_npm_build() {
    # Cleanup to be done:
    # - when script ends
    # - AND when script fails

    # clean node_modules dir which was created as root
    rm -rf node_modules

    # dist must be cleaned, but afterwards...
    # so we need to give all rights (because container is run as root)
    # do it only if dist dir exists
    [ -d dist ] && chmod -R 777 dist
    [ -d build/application ] && chmod -R 777 build/application
}

build_appImage_enabled() {
    if [[ "${APP_VERSION}" < "2022.10.00" ]]; then
      echo "APPIMAGE build check: skip build - package.json version (${APP_VERSION}) is lower than 2022.10.00"
      return 1
    fi
    
    return 0
}

main() {
    # Trap EXIT
    # - will trap clean exits
    # - and exit because of failure (because of the set -e above)
    trap cleanup_npm_build EXIT

    if [ -n "${APP_VERSION}" ]; then
        set +e
        npm --no-git-tag-version -f version  "${APP_VERSION}" >/dev/null 2>&1
        set -e
    fi

    npm ci

    # Launch tests with virtual X environment
    # xvfb-run npm run test:all
    npm test

    if [[ "$COMMAND" = "default" ]]; then
      npm run build
      npm run dist-win64
      npm run dist-linux64
      if build_appImage_enabled; then
        npm run build-appImage
      fi
    elif [[ "$COMMAND" = "fromsource" ]]; then
      npm run dist-buildFromPackage-win64
    elif [[ "$COMMAND" = "sourceonly" ]]; then
      npm run build
      npm run dist-packageOnly-win64
      npm run dist-packageOnly-linux64
    fi
}

APP_VERSION=$(grep -oP '(?<="version": ")([\d\.]+)(?=")' package.json)
main "${@}"
